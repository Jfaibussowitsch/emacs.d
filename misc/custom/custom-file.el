;;; custom-file.el ---                               -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Jacob Faibussowitsch

;; Author: Jacob Faibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(backward-delete-char-untabify-method 'hungry)
 '(blink-cursor-blinks -1)
 '(blink-cursor-mode t)
 '(blink-matching-delay 0.5)
 '(blink-matching-paren t)
 '(connection-local-criteria-alist
   '(((:application tramp :machine "visitor097-020.wl.anl-external.org")
      tramp-connection-local-darwin-ps-profile)
     ((:application tramp :machine "Jacobs-MacBook-Pro.local")
      tramp-connection-local-darwin-ps-profile)
     ((:application tramp :protocol "flatpak")
      tramp-container-connection-local-default-flatpak-profile)
     ((:application tramp :machine "localhost") tramp-connection-local-darwin-ps-profile)
     ((:application tramp :machine "Jacobs-MBP.fios-router.home")
      tramp-connection-local-darwin-ps-profile)
     ((:application tramp) tramp-connection-local-default-system-profile
      tramp-connection-local-default-shell-profile)))
 '(connection-local-profile-alist
   '((tramp-container-connection-local-default-flatpak-profile
      (tramp-remote-path "/app/bin" tramp-default-remote-path "/bin" "/usr/bin" "/sbin"
                         "/usr/sbin" "/usr/local/bin" "/usr/local/sbin" "/local/bin"
                         "/local/freeware/bin" "/local/gnu/bin" "/usr/freeware/bin"
                         "/usr/pkg/bin" "/usr/contrib/bin" "/opt/bin" "/opt/sbin"
                         "/opt/local/bin"))
     (tramp-connection-local-darwin-ps-profile
      (tramp-process-attributes-ps-args "-acxww" "-o"
                                        "pid,uid,user,gid,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "-o" "state=abcde" "-o"
                                        "ppid,pgid,sess,tty,tpgid,minflt,majflt,time,pri,nice,vsz,rss,etime,pcpu,pmem,args")
      (tramp-process-attributes-ps-format (pid . number) (euid . number) (user . string)
                                          (egid . number) (comm . 52) (state . 5)
                                          (ppid . number) (pgrp . number) (sess . number)
                                          (ttname . string) (tpgid . number)
                                          (minflt . number) (majflt . number)
                                          (time . tramp-ps-time) (pri . number)
                                          (nice . number) (vsize . number) (rss . number)
                                          (etime . tramp-ps-time) (pcpu . number)
                                          (pmem . number) (args)))
     (tramp-connection-local-busybox-ps-profile
      (tramp-process-attributes-ps-args "-o"
                                        "pid,user,group,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "-o" "stat=abcde" "-o"
                                        "ppid,pgid,tty,time,nice,etime,args")
      (tramp-process-attributes-ps-format (pid . number) (user . string) (group . string)
                                          (comm . 52) (state . 5) (ppid . number)
                                          (pgrp . number) (ttname . string)
                                          (time . tramp-ps-time) (nice . number)
                                          (etime . tramp-ps-time) (args)))
     (tramp-connection-local-bsd-ps-profile
      (tramp-process-attributes-ps-args "-acxww" "-o"
                                        "pid,euid,user,egid,egroup,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "-o"
                                        "state,ppid,pgid,sid,tty,tpgid,minflt,majflt,time,pri,nice,vsz,rss,etimes,pcpu,pmem,args")
      (tramp-process-attributes-ps-format (pid . number) (euid . number) (user . string)
                                          (egid . number) (group . string) (comm . 52)
                                          (state . string) (ppid . number) (pgrp . number)
                                          (sess . number) (ttname . string)
                                          (tpgid . number) (minflt . number)
                                          (majflt . number) (time . tramp-ps-time)
                                          (pri . number) (nice . number) (vsize . number)
                                          (rss . number) (etime . number) (pcpu . number)
                                          (pmem . number) (args)))
     (tramp-connection-local-default-shell-profile (shell-file-name . "/bin/sh")
                                                   (shell-command-switch . "-c"))
     (tramp-connection-local-default-system-profile (path-separator . ":")
                                                    (null-device . "/dev/null"))))
 '(custom-safe-themes
   '("5875d45a8f7cd5410c3cccc0d2139c62bb4be52c15f147684f66eefc597c4fb2" default))
 '(jit-lock-stealth-time 2)
 '(jka-compr-compression-info-list
   '(["\\.Z\\'" "compressing" "compress" ("-c") "uncompressing" "gzip" ("-c" "-q" "-d") nil t
      "\37\235" zlib-decompress-region]
     ["\\.bz2\\'" "bzip2ing" "bzip2" nil "bunzip2ing" "bzip2" ("-d") nil t "BZh" nil]
     ["\\.tbz2?\\'" "bzip2ing" "bzip2" nil "bunzip2ing" "bzip2" ("-d") nil nil "BZh" nil]
     ["\\.\\(?:tgz\\|svgz\\|sifz\\)\\'" "compressing" "gzip" ("-c" "-q") "uncompressing"
      "gzip" ("-c" "-q" "-d") t nil "\37\213" zlib-decompress-region]
     ["\\.g?z\\'" "compressing" "gzip" ("-c" "-q") "uncompressing" "gzip" ("-c" "-q" "-d")
      t t "\37\213" zlib-decompress-region]
     ["\\.lz\\'" "Lzip compressing" "lzip" ("-c" "-q") "Lzip uncompressing" "lzip"
      ("-c" "-q" "-d") t t "LZIP" nil]
     ["\\.lzma\\'" "LZMA compressing" "lzma" ("-c" "-q" "-z") "LZMA uncompressing" "lzma"
      ("-c" "-q" "-d") t t "" nil]
     ["\\.xz\\'" "XZ compressing" "xz" ("-c" "-q") "XZ uncompressing" "xz"
      ("-c" "-q" "-d") t t "\3757zXZ\0" nil]
     ["\\.txz\\'" "XZ compressing" "xz" ("-c" "-q") "XZ uncompressing" "xz"
      ("-c" "-q" "-d") t nil "\3757zXZ\0" nil]
     ["\\.dz\\'" nil nil nil "uncompressing" "gzip" ("-c" "-q" "-d") nil t "\37\213" nil]
     ["\\.zst\\'" "zstd compressing" "zstd" ("-c" "-q") "zstd uncompressing" "zstd"
      ("-c" "-q" "-d") t t "(\265/\375" nil]
     ["\\.tzst\\'" "zstd compressing" "zstd" ("-c" "-q" "-T4") "zstd uncompressing" "zstd"
      ("-c" "-q" "-d") t nil "(\265/\375" nil]))
 '(package-archives
   '(("gnu" . "http://elpa.gnu.org/packages/") ("melpa" . "http://melpa.org/packages/")))
 '(package-selected-packages
   '(all-the-icons async auctex auto-compile auto-complete auto-complete-config avy c-ts-mode
                   cc-mode clang-format clang-format+ cmake-mode comint compile counsel
                   counsel-projectile crux ctags-update cython-mode dash dashboard
                   deadgrep diminish dired dockerfile-ts-mode doom-modeline etags-select
                   etags-table f filladapt flycheck flyspell hideshow
                   highlight-parentheses hl-line+ hydra ibuffer ibuffer-projectile ivy
                   ivy-hydra lsp-ivy lsp-mode magit markdown-mode menu-bar midnight
                   move-lines mwim org ppindent python python-black recentf rustic s
                   saveplace scroll-bar simpleclip smerge-mode swiper tempbuf tex tool-bar
                   tramp tree-sitter tree-sitter-hl tree-sitter-langs treesit undo-tree vc
                   visual-regexp visual-regexp-steroids which-key ws-butler xref yaml-mode
                   yasnippet zop-to-char))
 '(safe-local-variable-values
   '((eval add-hook 'before-save-hook #'delete-trailing-whitespace)
     (python-interpreter seq-find (lambda (item) (executable-find item))
                         '("python3" "python"))
     (header-auto-update-enabled)
     (c-offsets-alist (inexpr-class . +) (inexpr-statement . +) (lambda-intro-cont . +)
                      (inlambda . 0) (template-args-cont first c-lineup-template-args +)
                      (incomposition . +) (inmodule . +) (innamespace . 0)
                      (inextern-lang . +) (composition-close . 0) (module-close . 0)
                      (namespace-close . 0) (extern-lang-close . 0) (composition-open . 0)
                      (module-open . 0) (namespace-open . 0) (extern-lang-open . 0)
                      (objc-method-call-cont first c-lineup-ObjC-method-call-colons
                                             c-lineup-ObjC-method-call +)
                      (objc-method-args-cont . c-lineup-ObjC-method-args)
                      (objc-method-intro . [0]) (friend . 0)
                      (cpp-define-intro first c-lineup-cpp-define +) (cpp-macro-cont . +)
                      (cpp-macro . [0]) (inclass . +) (stream-op . c-lineup-streamop)
                      (arglist-cont-nonempty first jf/c-lineup-ternary-bodies
                                             c-lineup-gcc-asm-reg c-lineup-arglist)
                      (arglist-cont first jf/c-lineup-ternary-bodies c-lineup-gcc-asm-reg
                                    0)
                      (arglist-intro . +) (catch-clause . 0) (else-clause . 0)
                      (do-while-closure . 0) (label . 2) (access-label . -)
                      (substatement-label . 2) (substatement . +)
                      (statement-case-open . 0) (statement-case-intro . +)
                      (statement-block-intro . +)
                      (statement-cont first jf/c-lineup-ternary-bodies +) (statement . 0)
                      (brace-entry-open . 0) (brace-list-entry . 0) (brace-list-intro . +)
                      (brace-list-close . 0) (brace-list-open . 0) (block-close . 0)
                      (inher-cont . c-lineup-multi-inher) (inher-intro . +)
                      (member-init-cont . c-lineup-multi-inher) (member-init-intro . +)
                      (annotation-var-cont . +) (annotation-top-cont . 0)
                      (topmost-intro-cont . c-lineup-topmost-intro-cont)
                      (topmost-intro . 0) (knr-argdecl . 0) (func-decl-cont . +)
                      (inline-close . 0) (inline-open . 0) (class-close . 0)
                      (class-open . 0) (defun-block-intro . +) (defun-close . 0)
                      (defun-open . 0) (string . c-lineup-dont-change) (arglist-close . 0)
                      (substatement-open . 0) (case-label . 0) (block-open . 0)
                      (c . c-lineup-C-comments) (comment-intro . 0)
                      (knr-argdecl-intro . -))
     (c-offsets-alist (inexpr-class . +) (inexpr-statement . +) (lambda-intro-cont . +)
                      (inlambda . c-lineup-inexpr-block)
                      (template-args-cont c-lineup-template-args +) (incomposition . +)
                      (inmodule . +) (innamespace . +) (inextern-lang . +)
                      (composition-close . 0) (module-close . 0) (namespace-close . 0)
                      (extern-lang-close . 0) (composition-open . 0) (module-open . 0)
                      (namespace-open . 0) (extern-lang-open . 0)
                      (objc-method-call-cont c-lineup-ObjC-method-call-colons
                                             c-lineup-ObjC-method-call +)
                      (objc-method-args-cont . c-lineup-ObjC-method-args)
                      (objc-method-intro . [0]) (friend . 0)
                      (cpp-define-intro c-lineup-cpp-define +) (cpp-macro-cont . +)
                      (cpp-macro . [0]) (inclass . +) (stream-op . c-lineup-streamop)
                      (arglist-cont-nonempty c-lineup-gcc-asm-reg c-lineup-arglist)
                      (arglist-cont c-lineup-gcc-asm-reg 0) (arglist-intro . +)
                      (catch-clause . 0) (else-clause . 0) (do-while-closure . 0)
                      (label . 2) (access-label . -) (substatement-label . 2)
                      (substatement . +) (statement-case-open . 0)
                      (statement-case-intro . +) (statement-block-intro . +)
                      (statement-cont . +) (statement . 0) (brace-entry-open . 0)
                      (brace-list-entry . 0) (brace-list-intro . +) (brace-list-close . 0)
                      (brace-list-open . 0) (block-close . 0)
                      (inher-cont . c-lineup-multi-inher) (inher-intro . +)
                      (member-init-cont . c-lineup-multi-inher) (member-init-intro . +)
                      (annotation-var-cont . +) (annotation-top-cont . 0)
                      (topmost-intro-cont . c-lineup-topmost-intro-cont)
                      (topmost-intro . 0) (knr-argdecl . 0) (func-decl-cont . +)
                      (inline-close . 0) (inline-open . +) (class-close . 0)
                      (class-open . 0) (defun-block-intro . +) (defun-close . 0)
                      (defun-open . 0) (string . c-lineup-dont-change)
                      (arglist-close . c-lineup-arglist) (substatement-open . 0)
                      (case-label . 0) (block-open . 0) (c . 2) (comment-intro . 0)
                      (knr-argdecl-intro . -))
     (c-cleanup-list scope-operator brace-else-brace brace-elseif-brace brace-catch-brace
                     empty-defun-braces list-close-comma defun-close-semi)
     (c-hanging-semi&comma-criteria c-semi&comma-no-newlines-before-nonblanks)
     (c-hanging-colons-alist (member-init-intro before) (inher-intro) (case-label after)
                             (label after) (access-label after))
     (c-hanging-braces-alist (substatement-open after) (brace-list-open after)
                             (brace-entry-open) (defun-open after) (class-open after)
                             (inline-open after) (block-open after)
                             (block-close . c-snug-do-while) (statement-case-open after)
                             (substatement after))
     (c-block-comment-prefix . "") (c-comment-only-line-offset . 0)
     (c-tab-always-indent . t)))
 '(sh-basic-offset 2)
 '(tramp-use-connection-share nil nil nil "Customized with use-package tramp")
 '(warning-suppress-log-types '((comp) (comp)))
 '(warning-suppress-types '((comp))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-group-tag ((t (:inherit variable-pitch :foreground "mediumturquoise" :weight bold :height 1.2))))
 '(custom-variable-tag ((t (:foreground "darkturquoise" :weight bold))))
 '(error ((t (:foreground "Red1" :weight normal))))
 '(font-latex-script-char-face ((t (:foreground "darkorange"))))
 '(font-lock-comment-delimiter-face ((t (:foreground "orangered3" :slant italic))))
 '(font-lock-comment-face ((t (:foreground "orangered3" :slant italic))))
 '(font-lock-number-face ((t (:inherit font-lock-constant-face))))
 '(font-lock-operator-face ((t (:inherit font-lock-keyword-face))))
 '(font-lock-preprocessor-face ((t (:foreground "#0072b4"))))
 '(font-lock-property-face ((t (:inherit font-lock-variable-name-face :foreground "deeppink" :underline t))))
 '(font-lock-type-face ((t (:foreground "green"))))
 '(font-lock-variable-name-face ((t (:foreground "white"))))
 '(help-key-binding ((t (:background "grey96" :foreground "firebrick2" :box (:line-width (1 . -1) :color "grey80")))))
 '(highlight ((t (:background "yellow" :foreground "black"))))
 '(link ((t (:foreground "deepskyblue" :underline t))))
 '(minibuffer-prompt ((t (:foreground "mediumturquoise"))))
 '(rainbow-delimiters-depth-1-face ((t (:inherit rainbow-delimiters-base-face :foreground "brightwhite"))))
 '(rst-level-1 ((t (:background "grey50"))))
 '(rst-level-2 ((t (:background "grey50"))))
 '(rst-level-3 ((t (:background "grey50"))))
 '(rst-level-4 ((t (:background "grey50"))))
 '(rst-level-5 ((t (:background "grey50"))))
 '(success ((t (:foreground "green" :weight bold)))))

(provide 'custom-file)
;;; custom-file.el ends here
