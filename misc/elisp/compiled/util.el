;;; util.el --- Utility Functions      -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(eval-when-compile
  (defvar user-elisp-compiled-dir) ; defined in ~/.emacs.d/init.el
  (defvar user-setup-files-dir) ; defined in ~/.emacs.d/init.el
  (require 'cl-lib)
  (require 'dash)
  (defmacro jf/is-system-type-p (name &rest namelist)
    "Shorthand for determninig system-type"
    (if namelist
	`(--any-p (string= system-type it) (list ,name ,@namelist))
      (string= system-type name))))

(require 'f)

;;;###autoload
(defsubst jf/numcores ()
  "Return the number of logical processors on this system."
  (interactive)
  (cond
   ;; BSD+OSX
   ((jf/is-system-type-p "darwin")
    (with-temp-buffer
      (when (zerop (call-process "sysctl" nil t nil "-n" "hw.ncpu"))
	(string-to-number (buffer-string)))))
   ;; Linux
   ((jf/is-system-type-p "gnu" "gnu/linux" "gnu/kfreebsd")
    (with-temp-buffer
      (insert-file-contents "/proc/cpuinfo")
      (how-many "^processor[[:space:]]+:")))
   ;; Windows
   ((jf/is-system-type-p "ms-dos" "windows-nt" "cygwin")
    (let ((number-of-processors (getenv "NUMBER_OF_PROCESSORS")))
      (when number-of-processors
	(string-to-number number-of-processors))))
    ;; Default
    (t 1)))

(defmacro jf/time-func-call (&rest body)
  "Measure and return the time it takes evaluating BODY."
  `(let ((time (current-time)))
     ,@body
     (float-time (time-since time))))

(defmacro jf/print-time-func-call (&rest body)
  "Pretty-print output of jf/time-func-call."
  `(message "Function has run for %0.0f seconds" (jf/time-func-call ,@body)))

;;;###autoload
(defvar jf/gc-timer
  (run-with-idle-timer 10 t
                       (lambda ()
			 (let ((inhibit-message t))
                           (message "Garbage Collector has run for %.06f seconds"
                                    (jf/time-func-call (garbage-collect))))))
  "When idle for 10s run the GC no matter what")

;;;###autoload
(defun jf/buffer-mode (&optional buffer-or-name)
  "Returns the major mode associated with a buffer.
If buffer-or-name is nil return current buffer's mode."
  (interactive)
  (buffer-local-value 'major-mode
		      (if buffer-or-name (get-buffer buffer-or-name) (current-buffer))))

;;;###autoload
(defun jf/derived-mode-p (mode-name-var &optional buffer-or-name)
  "Does a buffer derive from another major mode"
  (interactive)
  (if buffer-or-name
      (progn(message "buffer or name exists")
      (with-current-buffer buffer-or-name
	(derived-mode-p mode-name-var)))
    (progn(message "buffer or name does not exist")
     (derived-mode-p mode-name-var))))

;;;###autoload
(defun jf/run-current-file (&optional eval-init)
  "Execute the current file.
If EVAL-INIT is non-nil, load the `init.el'.
For example, if the current buffer is the file xx.py, then it'll call
Òpython xx.pyÓ in a shell. The file can be php, perl, python, ruby,
javascript, bash, ocaml, vb, elisp.  File extension is used to determine
what program to run.
If the file is modified, user is asked to save it first.
If the buffer major-mode is `emacs-lisp-mode', run `eval-buffer'.
If the buffer major-mode is `clojure-mode', run `cider-load-buffer'."
  (interactive "P")
  (if eval-init
      (load (locate-user-emacs-file "init.el"))
    (progn
      (when (buffer-modified-p)
        (when (y-or-n-p "Buffer modified. Do you want to save first?")
          (save-buffer)))

      (cond
       ((derived-mode-p 'emacs-lisp-mode)
        (eval-buffer)                   ;Also works for .el.gz and .el.gpg files
        (message "Evaluated `%0s'." (buffer-name)))
       (t
        (let* ((suffix-map '(("py" . "python")
                             ("rb" . "ruby")
                             ("sh" . "bash")
                             ("csh" . "tcsh")
                             ("pl" . "perl")
                             ("tex" . "pdflatex")
                             ("latex" . "pdflatex")
                             ("d" . "dmd -de -w -unittest -run")
                             ("nim" . "nim c -r --verbosity:0")))
               (file-name (progn
                            ;; Save buffer as a file if it's not already a file.
                            (when (null (buffer-file-name)) (save-buffer))
                            (buffer-file-name)))
               (file-ext (file-name-extension file-name))
               (prog-name (cdr (assoc file-ext suffix-map)))
               (cmd-str (concat prog-name " \"" file-name "\"")))
          (if prog-name
              (progn
                ;; (view-echo-area-messages)
                (message "Running ...")
                (shell-command cmd-str "*run-current-file output*" ))
            (message "No recognized program file suffix for this file."))))))))

(defun jf/byte-recompile-all ()
  "Force byte-compile every `.el' file in `package-user-dir' and
`use-elisp-compiler-dir'. The `.el' files are re-compiled even if
the corresponding `.elc' files exist, in all the sub-directories
under `package-user-dir'. If the `.elc' file does not exist, this
function *does not* compile the corresponding `.el' file."
  (interactive)
  (byte-recompile-directory package-user-dir nil :force)
  (byte-recompile-directory user-elisp-compiled-dir nil :force))

(defun jf/byte-recompile-setup-dir ()
  "Byte-compile all your dotfiles."
  (interactive)
  (byte-recompile-directory user-setup-files-dir nil :force))

;;;###autoload
(defun jf/was-compiled-p (path)
  "Does the directory at PATH contain only .elc files?"
  (--all-p (f-ext? it "elc") (f-files path)))

;;;###autoload
(defun jf/rename-current-buffer-file ()
  "Renames current buffer and file it is visiting."
  (interactive)
  (let* ((name (buffer-name))
         (filename (buffer-file-name))
         (basename (file-name-nondirectory filename)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (let ((new-name (read-file-name "New name: " (file-name-directory filename) basename nil basename)))
        (if (get-buffer new-name)
            (error "A buffer named '%s' already exists!" new-name)
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil)
          (message "File '%s' successfully renamed to '%s'"
                   name (file-name-nondirectory new-name)))))))

(provide 'util)
;;; util.el ends here
