;;; setup-crux.el ---                                -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Jacob Faibussowitsch

;; Author: Jacob Faibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; crux
(use-package-select crux
  :defer
  :diminish
  :bind
  (("C-c d" . 'crux-duplicate-current-line-or-region)
   ("C-c n" . 'crux-cleanup-buffer-or-region)))

(provide 'setup-crux)
;;; setup-crux.el ends here
