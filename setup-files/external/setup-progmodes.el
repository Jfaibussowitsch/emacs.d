;;; setup-progmodes.el ---                           -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MacBook-Pro.local>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(defun jf/insert-chkerr-func--internal (begin end chkerr_func)
  "Wrap a region BEGIN and END in CHKERR_FUNC()"
  ;; make sure we are at the start of region
  (goto-char begin)
  (let ((chkerr_func_str (funcall chkerr_func begin end)))
    (unless (string-equal (thing-at-point 'symbol) chkerr_func_str)
      (insert-before-markers chkerr_func_str ?\()
      (goto-char (+ end 1 (length chkerr_func_str)))
      (insert-char ?\))
      (unless (char-equal (following-char) ?\;)
        (insert-char ?\;)))))

(defun jf/insert-chkerr-func (chkerr_func)
  "Intelligently wrap current line in CHKERR_FUNC()"
  (interactive)
  (save-excursion
    (let ((using-region (use-region-p)))
      (unless using-region
        ;; go to start of indented line
        (back-to-indentation)
        (let ((eol (line-end-position)))
          ;; skip any chars that are:
          ;; ^ - NOT
          ;; w - word constituent (including numbers)
          ;; _ - symbol constituent
          (skip-syntax-forward "^w_" eol)
          ;; special handling of keywords
          (while (looking-at-p "\\(for\\|while\\|switch\\|do\\|if\\|else\\|case\\)\s+")
            ;; special handling here, forward-list would skip to EOL for these
            (if (looking-at-p "\\(do\\|else\\|case\\)\s+")
                (forward-word)
              (forward-list))
            (skip-syntax-forward "^w_" eol)))
        ;; set start of region
        (push-mark nil t t)
        ;; skip to end of symbol
        (forward-list)
        ;; ...unless symbol is a template, then the point will land right after the
        ;; template bracket closer:
        ;;
        ;;  MyTemplateFunction<foo, bar, baz>(foo, bar, baz)
        ;; ^                                ^               ^
        ;; |                                |               |
        ;; starts here                      lands here      destination
        ;;
        ;; so we need one final forward-list to get to our destination
        (when (char-equal (preceding-char) ?>)
          (forward-list)))
      (with-demoted-errors "ERROR wrapping line in CHKERR_FUNC: %s"
        (jf/insert-chkerr-func--internal (region-beginning) (region-end) chkerr_func))
      (unless using-region (pop-mark)))
    (indent-according-to-mode)))

(use-package-select cmake-mode
  :diminish
  :defer)

(use-package-select cython-mode
  :diminish
  :defer)

(use-package-select yaml-mode
  :diminish
  :mode ("\\.yml\\'" "\\.clang-tidy\\'" "\\.clang-format\\'"))

(use-package-select markdown-mode
  :diminish
  :mode (("\\.md\\'" . markdown-mode)))

(use-package-select cc-mode
  :ensure nil ;; built-in
  :defer
  :init
  ;; register file suffixes
  (add-to-list 'auto-mode-alist '("\\.C\\'"         . c++-mode))
  (add-to-list 'auto-mode-alist '("\\.cxx\\'"       . c++-mode))
  (add-to-list 'auto-mode-alist '("\\.cu\\'"        . c++-mode))
  (add-to-list 'auto-mode-alist '("\\.cuh\\'"       . c++-mode))
  (add-to-list 'auto-mode-alist '("\\.cuinl\\'"     . c++-mode))
  (add-to-list 'auto-mode-alist '("\\.modulemap\\'" . c++-mode))
  (add-to-list 'auto-mode-alist '("\\.inl\\'"       . c++-mode))
  (add-to-list 'auto-mode-alist '("/libcudacxx/include/.*$" . c++-mode))
  :bind (:map c-mode-base-map
              ("C-c c" . (lambda () (interactive) (jf/insert-chkerr-func 'jf/insert-petsccall))))
  :hook
  (c-mode-common . jf/setup-cc-mode)
  :config
  (defconst cuda-mode-keywords
    '(("__global__"      . font-lock-keyword-face)
      ("__shared__"      . font-lock-keyword-face)
      ("__device__"      . font-lock-keyword-face)
      ("__forceinline__" . font-lock-keyword-face)
      ("__host__"        . font-lock-keyword-face)
      ("__constant__"    . font-lock-keyword-face)))

  (defconst petscdoc-font-lock-doc-comments
    `(("seealso:" ; "foo: ..." markup.
       3 ,c-doc-markup-face-name prepend nil)))

  (defconst petscdoc-font-lock-keywords
    `((,(lambda (limit)
	  (c-font-lock-doc-comments "/\\*\\*" limit petscdoc-font-lock-doc-comments)))))

  (defun jf/c-lineup-ternary-bodies (&rest args)
    (>=e "28"
        (c-lineup-ternary-bodies args)))

  (defun jf/insert-petsccall (begin end)
    (pcase (buffer-substring-no-properties begin end)
      ((rx bos "MPI"       ) "PetscCallMPI"       )
      ((rx bos "cuda"      ) "PetscCallCUDA"      )
      ((rx bos "cublas"    ) "PetscCallCUBLAS"    )
      ((rx bos "cusolver"  ) "PetscCallCUSOLVER"  )
      ((rx bos "hipblas"   ) "PetscCallHIPBLAS"   )
      ((rx bos "hipsolver" ) "PetscCallHIPSOLVER" )
      ((rx bos "hip"       ) "PetscCallHIP"       )
      ((rx bos "cupmblas"  ) "PetscCallCUPMBLAS"  )
      ((rx bos "cupmsolver") "PetscCallCUPMSOLVER")
      ((rx bos "cupm"      ) "PetscCallCUPM"      )
      ((rx bos "std::"     ) "PetscCallCXX"       )
      ;; default
      (_                     "PetscCall"          )))

  (defun jf/setup-cc-mode ()
    (c-toggle-auto-newline 1)
    (subword-mode 1)
    (setq c-noise-macro-names '("PETSC_NODISCARD"
				"PETSC_CONSTEXPR_17"
				"PETSC_CONSTEXPR_14"
				"PETSC_CONSTEXPR"
				"PETSC_STATIC_INLINE"
				"PETSC_INLINE"
				"PETSC_NOEXCEPT"
				"PETSC_VISIBILITY_INTERNAL"
				"PETSC_VISIBILITY_EXTERNAL"
                                "PETSC_EXTERN"
                                "PETSC_INTERN"))
    (c-set-style "jf-cc-mode-style")
    (c-make-noise-macro-regexps)
    (c-setup-doc-comment-style))

  (mapc
   (lambda (mode)
     (font-lock-add-keywords mode cuda-mode-keywords))
   '(c-mode c++-mode))

  (c-add-style
   "jf-cc-mode-style"
   '((c-tab-always-indent	 . t)
     (c-basic-offset       	 . 2)
     (c-comment-only-line-offset . 0)
     (fill-column                . 95)
     (c-backslash-max-column     . 95)
     (c-block-comment-prefix     . "*")
     (c-doc-comment-style        . doxygen)
     (c-hanging-braces-alist     . ((substatement-open     after)
				    (statement-block-intro after)
				    (brace-list-open       after)
				    (brace-entry-open           )
				    (inline-open        . (before after))
				    (block-open            after)
				    (defun-block-intro     after)
				    (block-close	.  c-snug-do-while)
				    (statement-case-open   after)
				    (substatement          after)))
     (c-hanging-colons-alist . ((member-init-intro before)
				(inher-intro             )
				(case-label         after)
				(label after             )
				(access-label       after)))
     (c-hanging-semi&comma-criteria . (c-semi&comma-no-newlines-before-nonblanks))
     (c-cleanup-list . (scope-operator
			brace-else-brace
			brace-elseif-brace
			brace-catch-brace
			empty-defun-braces
			list-close-comma
			defun-close-semi
                        one-liner-defun
                        compact-empty-funcall))
     (c-offsets-alist . ((inexpr-class          . +)
			 (inexpr-statement	. +)
			 (lambda-intro-cont	. +)
			 (inlambda		. 0)
			 (template-args-cont    . (first c-lineup-template-args
                                                         +))
			 (incomposition		. +)
			 (inmodule              . +)
			 (innamespace		. 0)
			 (inextern-lang		. +)
			 (composition-close	. 0)
			 (module-close		. 0)
			 (namespace-close	. 0)
			 (extern-lang-close	. 0)
			 (composition-open	. 0)
			 (module-open		. 0)
			 (namespace-open        . 0)
			 (extern-lang-open	. 0)
			 (objc-method-call-cont . (first c-lineup-ObjC-method-call-colons
                                                         c-lineup-ObjC-method-call
						         +))
			 (objc-method-args-cont	. c-lineup-ObjC-method-args)
			 (objc-method-intro	. [0])
			 (friend		. 0)
			 (cpp-define-intro      . (first c-lineup-cpp-define
                                                         +))
			 (cpp-macro-cont	. +)
			 (cpp-macro		. [0])
			 (inclass		. +)
			 (stream-op		. c-lineup-streamop)
			 (arglist-cont-nonempty . (first jf/c-lineup-ternary-bodies
                                                         c-lineup-gcc-asm-reg
						         c-lineup-arglist))
			 (arglist-cont          . (first jf/c-lineup-ternary-bodies
                                                         c-lineup-gcc-asm-reg
                                                         0))
			 (arglist-intro		. +)
			 (catch-clause		. 0)
			 (else-clause		. 0)
			 (do-while-closure	. 0)
			 (label			. 2)
			 (access-label		. -)
			 (substatement-label	. 2)
			 (substatement		. +)
			 (statement-case-open	. 0)
			 (statement-case-intro	. +)
			 (statement-block-intro	. +)
			 (statement-cont        . (first jf/c-lineup-ternary-bodies
                                                         +))
			 (statement		. 0)
			 (brace-entry-open	. 0)
			 (brace-list-entry	. 0)
			 (brace-list-intro	. +)
			 (brace-list-close	. 0)
			 (brace-list-open	. 0)
			 (block-close		. 0)
			 (inher-cont		. c-lineup-multi-inher)
			 (inher-intro		. +)
			 (member-init-cont	. c-lineup-multi-inher)
			 (member-init-intro	. +)
			 (annotation-var-cont	. +)
			 (annotation-top-cont	. 0)
			 (topmost-intro-cont	. c-lineup-topmost-intro-cont)
			 (topmost-intro		. 0)
			 (knr-argdecl		. 0)
			 (func-decl-cont	. +)
			 (inline-close		. 0)
			 (inline-open		. 0)
			 (class-close		. 0)
			 (class-open		. 0)
			 (defun-block-intro	. +)
			 (defun-close		. 0)
			 (defun-open		. 0)
			 (string		. c-lineup-dont-change)
			 (arglist-close		. 0)
			 (substatement-open	. 0)
			 (case-label		. 0)
			 (block-open		. 0)
			 (comment-intro		. 0)
			 (knr-argdecl-intro	. -)))))
  (when (listp c-default-style)
    (setf (alist-get 'other c-default-style) "jf-cc-mode-style")))

(use-package-select python
  :ensure nil ;; built-in
  :defer
  :hook
  (python-mode . jf/setup-python-mode)
  :bind (:map python-mode-map
	      ("C-c C-c" . (lambda () (interactive) (python-shell-send-buffer t)))
              ("C-c TAB" . #'jf/hideshow-toggle))
  :config
  (defun jf/setup-python-mode ()
    (subword-mode 1))
  :custom
  (python-interpreter            (seq-find (lambda (item) (executable-find item)) '("python3" "python")))
  (python-shell-interpreter      "ipython")
  (python-shell-interpreter-args "-i --simple-prompt"))

(use-package-select ruff-format
  :if (executable-find "ruff")
  :hook (python-mode . ruff-format-on-save-mode))

(use-package-select python-black
  :if (unless (executable-find "ruff")
        (executable-find "black"))
  :after python
  :hook (python-mode . python-black-on-save-mode))

(use-package-select rustic
  :mode ("\\.rs\\'" . rustic-mode)
  :config
  (defun jf/setup-rs-mode ()
    (subword-mode 1))

  ;; (jf/after lsp
  ;;   (lsp-register-client
  ;;    (make-lsp-client
  ;;     :new-connection (lsp-tramp-connection "rust-analyzer")
  ;;     :remote? t
  ;;     :major-modes '(rust-mode rustic-mode)
  ;;     :initialization-options 'lsp-rust-analyzer--make-init-options
  ;;     :notification-handlers (ht<-alist lsp-rust-notification-handlers)
  ;;     :action-handlers (ht ("rust-analyzer.runSingle" #'lsp-rust--analyzer-run-single))
  ;;     :library-folders-fn (lambda (_workspace) lsp-rust-analyzer-library-directories)
  ;;     :ignore-messages nil
  ;;     :server-id 'rust-analyzer-remote)))

  (add-hook 'rustic-mode-hook 'jf/setup-rs-mode)
  :custom
  (rust-prettify-symbols-alist   nil)
  (rustic-format-display-method 'ignore)
  (rustic-format-trigger        'on-save)
  (rustic-format-on-save-method 'rustic-format-buffer)
  (rust-indent-offset            2))

(use-package-select dockerfile-ts-mode
  :ensure nil ;; built-in
  :mode "\\Dockerfile\\'")

;; (use-package-select rust-ts-mode
;;   :mode ("\\.rs\\'" . rust-ts-mode)
;;   :ensure nil ;; built-in
;;   :defer
;;   ;; :disabled
;;   ;; :hook   rustic-mode
;;   :config
;;     (defun jf/setup-lsp-rs-ts-mode ()
;;     (lsp-register-client
;;      (make-lsp-client
;;       :new-connection (lsp-tramp-connection "rust-analyzer")
;;       :remote? t
;;       :major-modes '(rust-ts-mode)
;;       :initialization-options 'lsp-rust-analyzer--make-init-options
;;       :notification-handlers (ht<-alist lsp-rust-notification-handlers)
;;       :action-handlers (ht ("rust-analyzer.runSingle" #'lsp-rust--analyzer-run-single))
;;       :library-folders-fn (lambda (_workspace) lsp-rust-analyzer-library-directories)
;;       :ignore-messages nil
;;       :server-id 'rust-analyzer-remote)))

;;   (add-hook 'rust-ts-mode-hook 'jf/setup-rs-ts-mode)
;;   :custom
;;   (rust-ts-mode-indent-offset 2))

(provide 'setup-progmodes)
;;; setup-progmodes.el ends here
