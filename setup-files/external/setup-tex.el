;;; setup-tex.el ---                                 -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(add-to-list 'auto-mode-alist '("\\.tex\\'" . LaTeX-mode))

(use-package-select tex
  :defer t
  :ensure auctex
  :config
  ;; http://www.gnu.org/software/auctex/manual/auctex/Multifile.html
  (setq TeX-parse-self t ; parse on load
	TeX-auto-save t  ; parse on save
	TeX-auto-default (concat user-cache-dir "auctex/auto")
	TeX-auto-private (concat user-cache-dir "auctex/auto")
	TeX-style-private (concat user-cache-dir "auctex/style")
	;; use hidden dirs for auctex files
	TeX-auto-local ".auctex-auto"
	TeX-style-local ".auctex-style"
	TeX-source-correlate-mode t
	TeX-source-correlate-method 'synctex
	;; don't start the emacs server when correlating sources
	TeX-source-correlate-start-server nil
	;; just save, dont ask me before each compilation
	TeX-save-query nil
	TeX-PDF-mode   t
        TeX-clean-confirm nil)
  (setq-default TeX-output-dir "build")
  (setq LaTeX-electric-left-right-brace t)
  (setq TeX-electric-math t)
  (setq TeX-electric-sub-and-superscript nil)
  (add-hook 'plain-TeX-mode-hook
	    (lambda () (set (make-local-variable 'TeX-electric-math)
			    (cons "$" "$"))))
  (add-hook 'LaTeX-mode-hook
	    (lambda () (set (make-local-variable 'TeX-electric-math)
			    (cons "$" "$"))))
  (setcar (cdr (assoc "Check" TeX-command-list)) "chktex -v6 -H %s")
  ;; Enable word wrapping
  (add-hook 'TeX-mode-hook #'visual-line-mode)
  )

(provide 'setup-tex)
;;; setup-tex.el ends here
