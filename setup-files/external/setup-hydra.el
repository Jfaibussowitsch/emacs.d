;;; setup-hydra.el ---                               -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package-select hydra
  :diminish
  :config
  (bind-key
   (kbd "C-n")
   (defhydra hydra-move
     (:hint nil :body-pre (next-line))
     "
^      ^ ^ ^ ^Basic^ ^ ^         ^^^^^^| Extra
^------^-^-^-^-^-^-^-^-^-^-^-^-------^-+------------------------
^      ^ ^ ^ ^ ^ _P_ ^ ^ ^ ^ ^       ^ | _l_: recenter
^      ^ ^ ^ ^ ^ _p_ ^ ^ ^ ^ ^       ^ |
_<left>_ _a_ _b_ ^+^ _f_ _e_ _<right>_ |
^      ^ ^ ^ ^ ^ _n_ ^ ^ ^ ^ ^       ^ |
^      ^ ^ ^ ^ ^ _N_ ^ ^ ^ ^ ^       ^ |
"
     ("P" backward-paragraph)
     ("p" previous-line)
     ("n" next-line)
     ("N" forward-paragraph)
     ("f" forward-char)
     ("b" backward-char)
     ("a" beginning-of-line)
     ("e" move-end-of-line)
     ("<left>" previous-buffer)
     ("<right>" next-buffer)
     ("l" recenter-top-bottom))))

(provide 'setup-hydra)
;;; setup-hydra.el ends here
