;;; setup-tags.el ---                                -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; (defadvice find-tag (around refresh-etags activate compile)
;;   "Rerun etags and reload tags if tag not found and redo find-tag.
;;    If buffer is modified, ask about save before running etags."
;;   (let ((extension (file-name-extension (buffer-file-name))))
;;     (condition-case err
;;         ad-do-it
;;       (error (and (buffer-modified-p)
;;                   (not (ding))
;;                   (y-or-n-p "Buffer is modified, save it? ")
;;                   (save-buffer))
;;              (er-refresh-etags extension)
;;              ad-do-it))))

;;(defun er-refresh-etags (&optional extension)...)

(setq tags-revert-without-query t)

(use-package-select etags-select
  :ensure nil ;; misc/elisp/etags-select.el
  :if (executable-find "etags")
  :bind
  (("M-?" . etags-select-find-tag)
   ("M-." . etags-select-find-tag-at-point))
  :config
  (jf/defadvice etags-select-goto-tag--expand-after-goto-tag (&rest _)
    "hideshow-expand affected block when using M-. in a collapsed buffer"
    :after '(etags-select-goto-tag)
    (save-excursion (hs-show-block))))
  ;; (defadvice etags-select-goto-tag (after expand-after-goto-tag activate compile)
  ;;   "hideshow-expand affected block when using M-. in a collapsed buffer"
  ;;   (save-excursion (hs-show-block))))

(use-package-select etags-table
  :ensure nil ;; misc/elisp/etags-table.el
  :after etags-select
  :config
  (progn
    (setq etags-table-search-up-depth 99)
    (setq etags-table-alist nil) ; init
    ;;(setq tags-table-alist
    ;; For jumping across project:
    ;;'("/Users/jacobfaibussowitsch/NoSync/petsc/TAGS"
    ;;"/Users/jacobfaibussowitsch/NoSync/lammps/TAGS"
    ;;"/usr/local/include/TAGS"))
    (add-to-list 'etags-table-alist
                 `(,(concat user-emacs-directory ".*")
                   ,(concat user-emacs-directory "TAGS")))))

(use-package-select ctags-update
  :after etags-table
  :config
  (progn
    ;; Auto update
    (setq ctags-update-delay-seconds (* 30 60)) ; every 1/2 hour
    (defvar ctags-options-file
      (let ((file (expand-file-name ".ctags" user-home-directory)))
        (when (file-exists-p file)
          file))
      "User's Ctags options file.")

    (when ctags-options-file
      (setq ctags-update-other-options
            (list (concat "--options=" ctags-options-file))))))

(provide 'setup-tags)
;;; setup-tags.el ends here
