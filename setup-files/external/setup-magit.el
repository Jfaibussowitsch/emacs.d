;;; setup-magit.el ---                               -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Jacob Faibussowitsch

;; Author: Jacob Faibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
;; magit
(use-package-select magit
  :commands (magit-status magit-status)
  :defer-incrementally (dash f s smerge-mode with-editor git-commit package eieio transient)
  :diminish
  :delight magit-revert-buffers
  :bind
  ("C-x g" . magit-status)
  :custom
  (transient-levels-file  (concat user-cache-dir "transient/levels.cache"))
  (transient-values-file  (concat user-cache-dir "transient/values.cache"))
  (transient-history-file (concat user-cache-dir "transient/history.cache"))
  (magit-git-executable (executable-find "git"))
  (transient-default-level 5)
  ;; show granular diffs in selected hunk
  (magit-diff-refine-hunk t)
  ;; Don't autosave repo buffers. This is too magical, and saving can
  ;; trigger a bunch of unwanted side-effects, like save hooks and
  ;; formatters. Trust the user to know what they're doing.
  (magit-save-repository-buffers nil)
  ;; Don't display parent/related refs in commit buffers; they are rarely
  ;; helpful and only add to runtime costs.
  (magit-revision-insert-related-refs nil)
  :custom-face
  (magit-blame-highlight ((t (:background "brightblack" :foreground "black"))))
  (magit-diff-added ((t (:background "color-22" :foreground "#22aa22"))))
  (magit-section-highlight ((t (:background "brightblack")))))

(provide 'setup-magit)
;;; setup-magit.el ends here
