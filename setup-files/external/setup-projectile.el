;;; setup-projectile.el ---                          -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(use-package-select projectile
  :defer t
  :commands (projectile-command-map
	     projectile-project-root
             projectile-project-name
             projectile-project-p
             projectile-locate-dominating-file
             projectile-relevant-known-projects)
  :init
  (setq projectile-known-projects-file (concat user-cache-dir "projectile_projects.cache"))
  :config
  (add-to-list 'projectile-globally-ignored-directories "^\\.DS_Store$" t)
  (add-to-list 'projectile-globally-ignored-directories "^arch\\-.*" t)
  (add-to-list 'projectile-globally-ignored-directories "^venv.*" t)
  (add-to-list 'projectile-globally-ignored-directories "^test_venv$" t)
  (add-to-list 'projectile-globally-ignored-directories "^install.*" t)
  (add-to-list 'projectile-globally-ignored-directories "^build$" t)
  (projectile-mode +1)
  ;; once you have selected your project, the top-level directory of the project is
  ;; immediately opened for you in a dired buffer.
  (setq )
  (jf/defadvice jf/ensure-projectile-cache-writeable (data filename &rest _)
    "Ensure the project cache file is writeable by making the directories"
    :before '(projectile-serialize)
    (when (and
           (not (file-exists-p filename))
           (string= filename (projectile-project-cache-file)))
      (message "Projectile cache DNE, ensuring parent directories exist: %s" filename)
      (make-directory (file-name-directory filename))))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :custom
  (projectile-enable-caching     t)
  (projectile-cache-file         ".cache/projectile/projectile.cache")
  (projectile-completion-system 'ivy)
  ;;(projectile-enable-idle-timer t)
  (projectile-per-project-compilation-buffer t)
  (projectile-globally-ignored-file-suffixes '(".elc" ".eln" ".pyc" ".o"))
  (projectile-switch-project-action   #'projectile-dired)
  (compilation-buffer-name-function   #'projectile-compilation-buffer-name)
  (compilation-save-buffers-predicate #'projectile-current-project-buffer-p))

(use-package-select counsel-projectile
  :after (projectile counsel))

(provide 'setup-projectile)
;;; setup-projectile.el ends here
