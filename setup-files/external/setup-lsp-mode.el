;;; setup-lsp-mode.el ---                            -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:
(use-package-select lsp-mode
  :commands (lsp lsp-deferred)
  :defer-incrementally flycheck flyspell
  :hook
  ;; add more major mode as desired (e.g. python-mode, sh-mode)
  ((c-mode c-ts-mode c++-mode c++-ts-mode rust-ts-mode) . lsp-deferred)
  ;; if you want which-key integration
  (lsp-mode . lsp-enable-which-key-integration)
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix      "C-c l"
	lsp-session-file       (concat user-cache-dir "lsp.cache")
	lsp-server-install-dir (concat user-cache-dir "lsp-servers"))

  :config
  (when-let* ((llvm_dir (getenv "LLVM_DIR")))
    (setq lsp-clients-clangd-executable
          (locate-file "clangd" (ensure-list (concat (file-name-as-directory llvm_dir) "bin")))))

  (jf/after hideshow
    (jf/defadvice lsp-find-definition--expand-after-goto-tag (&rest _)
      "Hideshow-expand for (lsp-find-definition)."
      :after '(lsp-find-definition)
      (save-excursion (hs-show-block))))
    ;; (defadvice lsp-find-definition (after expand-after-goto-tag activate compile)
    ;;   "Hideshow-expand for (lsp-find-definition)."
  ;;   (save-excursion (hs-show-block))))
  (jf/after lsp-diagnostics
    (setq lsp-diagnostics-attributes
          (--map-first
           (eq (-first-item it) 'unnecessary)
           (-snoc it :strike-through t)
           lsp-diagnostics-attributes)))
  :custom
  (lsp-use-plists t)
  ;; 1MB
  (read-process-output-max (* 1024 1024))
  ;; if set to true can cause a performance hit
  (lsp-log-io nil)
  ;; auto restart LSP
  (lsp-restart 'auto-restart)
  (lsp-signature-render-documentation t)
  ;; header-line path
  (lsp-headerline-breadcrumb-enable t)
  ;; header-line icons
  (lsp-headerline-breadcrumb-icons-enable nil)
  ;; sideline code actions
  (lsp-ui-sideline-enable nil)
  ;; modeline code actions
  (lsp-modeline-code-actions-enable nil)
  ;; modeline diags (doom-modeline already provides this)
  (lsp-modeline-diagnostics-enable nil)
  ;; lsp builtin auto-fill
  (lsp-enable-on-type-formatting nil)
  ;; disable lenses
  (lsp-lens-enable nil)
  ;; completion
  (lsp-completion-enable nil)
  (lsp-completion-provider :none)
  ;; disable indentation
  (lsp-enable-indentation nil)
  ;;(setq lsp-completion-show-detail nil)
  ;;(setq lsp-completion-show-kind nil)
  ;;(setq lsp-semantic-tokens-apply-modifiers t)
  ;;(setq lsp-semantic-tokens-warn-on-missing-face t)
  (lsp-idle-delay 0.5)
  ;;(lsp-disabled-clients '(semgrep-ls))
  (lsp-clients-clangd-args
   `("--header-insertion=never"
     "--header-insertion-decorators"
     "-j=4"
     "--background-index"
     "--all-scopes-completion"
     "--enable-config"
     "--clang-tidy"
     "--include-cleaner-stdlib"))
  :custom-face
  (lsp-face-highlight-textual            ((t (:background "brightblue" :foreground "white"))))
  (lsp-face-highlight-read               ((t (:inherit lsp-face-highlight-textual :underline t))))
  (lsp-face-highlight-write              ((t (:inherit lsp-face-highlight-textual :weight bold))))
  (lsp-face-semhl-deprecated             ((t (:strike-through t))))
  (lsp-face-semhl-interface              ((t (:inherit lsp-face-semhl-function))))
  (lsp-face-semhl-member                 ((t (:inherit font-lock-variable-name-face :foreground "deeppink" :underline t :slant italic))))
  (lsp-face-semhl-method                 ((t (:inherit lsp-face-semhl-function :foreground "royalblue3"))))
  (lsp-face-semhl-namespace              ((t (:inherit font-lock-constant-face :weight bold))))
  (lsp-face-semhl-variable               ((t (:inherit font-lock-variable-name-face :foreground "white"))))
  (lsp-face-semhl-operator               ((t (:inherit lsp-face-semhl-keyword))))
  (lsp-face-semhl-parameter              ((t (:inherit lsp-face-semhl-variable))))
  (lsp-face-semhl-property               ((t (:inherit lsp-face-semhl-member :slant normal))))
  ;; (lsp-flycheck-error-unnecessary-face   ((t (:asdasd t))))
  ;; (lsp-flycheck-warning-unnecessary-face ((t (:inherit lsp-flycheck-error-unnecessary-face))))
  )                                                    

(use-package-select lsp-ivy
  :after ((:any lsp lsp-mode) ivy))

(provide 'setup-lsp-mode)
;;; setup-lsp-mode.el ends here
