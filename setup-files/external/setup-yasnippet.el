;;; setup-yasnippet.el ---                           -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(jf/defconst-directory
 jf/snippets-dir user-elisp-dir "snippets"
 "Default location for yasnippet snippets")

(use-package-select yasnippet
  :defer-incrementally eldoc easymenu help-mode
  :commands (yas-reload-all
             yas-minor-mode-on
             yas-expand
             yas-expand-snippet
             yas-lookup-snippet
             yas-insert-snippet
             yas-new-snippet
             yas-visit-snippet-file
             yas-activate-extra-mode
             yas-deactivate-extra-mode
             yas-maybe-expand-abbrev-key-filter)
  :hook
  ((c-mode-common python-mode snippet-mode) . (lambda ()
                                                (yas-reload-all)
                                                (yas-minor-mode-on)))
  ((lsp-mode . yas-minor-mode))
  :custom
  (yas-wrap-around-region t)
  (yas-snippet-dirs '(jf/snippets-dir))
  )

;;(use-package-select yasnippet-snippets :defer t)

(provide 'setup-yasnippet)
;;; setup-yasnippet.el ends here
