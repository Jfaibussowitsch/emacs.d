;;; setup-highlight-parentheses.el ---             -*- lexical-binding: t; -*-
;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MacBook-Pro.local>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;
(use-package-select highlight-parentheses
  :diminish
  :hook prog-mode
  :custom
  (highlight-parentheses-delay      0.05)
  (highlight-parentheses-attributes nil)
  (highlight-parentheses-background-colors
   '("brightwhite" "brightblack" "brightblack" "brightwhite" "brightblack" "brightwhite"))
  (highlight-parentheses-colors
   '("brightred" "brightgreen" "brightyellow" "brightmagenta" "brightcyan" "brightblue"))
  :custom-face
  (highlight-parentheses-highlight ((t (:weight extra-bold)))))

(provide 'setup-highlight-parentheses)
;;; setup-highlight-parentheses.el ends here
