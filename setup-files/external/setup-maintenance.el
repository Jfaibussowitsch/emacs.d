;;; setup-maintenance.el ---                            -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package-select midnight
  :ensure nil ;; built-in
  :defer-incrementally t
  :config
  (midnight-mode 1)
  (add-to-list 'clean-buffer-list-kill-never-regexps "\\*lsp-.*")
  (add-hook 'midnight-hook #'package-refresh-contents)
  (add-hook 'midnight-hook #'garbage-collect)
  :custom
  (clean-buffer-list-delay-general 1)
  )

(use-package-select tempbuf
  :ensure nil ;; misc/elisp/tempbuf.el
  :defer-incrementally t
  :hook
  (help-mode                   . turn-on-tempbuf-mode)
  (emacs-lisp-compilation-mode . turn-on-tempbuf-mode)
  (package-menu-mode           . turn-on-tempbuf-mode)
  (native-comp-limple-mode     . turn-on-tempbuf-mode))

(use-package-select recentf
  :ensure nil ;; built-in
  :defer-incrementally t
  :config
  (add-hook 'kill-emacs-hook #'recentf-cleanup)
  :custom
  (recentf-save-file       (concat user-cache-dir "recentf.cache"))
  (recentf-auto-cleanup    (if (daemonp) "11:00pm" 'never))
  (recentf-max-saved-items 200))

(use-package-select saveplace
  :ensure nil ;; built-in
  :hook (after-init . save-place-mode)
  :config
  (jf/defadvice save-place-find-file-hook--expand-after-goto-line (&rest _)
    "hideshow-expand affected block when using save-place in a collapsed buffer"
    :after '(save-place-find-file-hook)
    (save-excursion (hs-show-block)))
  ;; (defadvice save-place-find-file-hook (after expand-after-goto-line activate compile)
  ;;   "hideshow-expand affected block when using save-place in a collapsed buffer"
  ;;   (save-excursion (hs-show-block)))
  :custom
  (save-place-file (concat user-cache-dir "places.cache")))

(provide 'setup-maintenance)
;;; setup-maintenance.el ends here
