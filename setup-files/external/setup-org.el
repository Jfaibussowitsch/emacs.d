;;; setup-org.el ---                                 -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package-select org
  :ensure nil ;; builtin
  :disabled t
  :defer-incrementally t
  :init
  (progn
    (define-prefix-command 'jf/org-mode-keymap)
    (global-set-key (kbd "C-c o") 'jf/org-mode-keymap))
  :bind
  (:map jf/org-mode-keymap
	("a" . org-agenda))
  :config
  (progn
    (setq org-directory user-org-dir)
    (defvar jf/unified-org-agenda-file (expand-file-name "agenda.files" org-directory)
      "One file to contain a list of all Org agenda files.")
    (setq org-agenda-files jf/unified-org-agenda-file)
    (unless (file-exists-p jf/unified-org-agenda-file)
      ;; http://stackoverflow.com/a/14072295/1219634
      ;; touch `jf/one-org-agenda-file'
      (write-region ";;" :ignore jf/unified-org-agenda-file))
    ;; Fontify code in code blocks
    (setq org-src-fontify-natively t)
    ;; Display entities like \tilde, \alpha, etc in UTF-8 characters
    (setq org-pretty-entities t)
    ;; Render subscripts and superscripts in Org buffers
    (setq org-pretty-entities-include-sub-superscripts t)
    ; Single key command execution when at beginning of a headline
    (setq org-use-speed-commands t)
    ;; fold / overview  - collapse everything, show only level 1 headlines
    ;; content          - show only headlines
    ;; nofold / showall - expand all headlines except the ones with :archive:
    ;;                    tag and property drawers
    ;; showeverything   - same as above but without exceptions
    (setq org-startup-folded 'content)
    ;; https://orgmode.org/manual/Clean-view.html
    ;; Enable `org-indent-mode' on Org startup
    (setq org-startup-indented t)
    (setq org-todo-keywords '((sequence "TODO" "DONE" "SOMEDAY" "CANCELED")))
    (setq org-todo-keyword-faces
          '(("TODO"     . org-todo)
            ("SOMEDAY"  . (:foreground "black" :background "#FFEF9F"))
            ("CANCELED" . (:foreground "#94BFF3" :weight bold :strike-through t))
	    ("DONE"     . (:foreground "black" :background "#91ba31"))))
    ;; Block entries from changing state to DONE while they have children
    ;; that are not DONE - https://orgmode.org/manual/TODO-dependencies.html
    (setq org-enforce-todo-dependencies t)
    ;; C-a/C-e goes to text when in headline
    (setq org-special-ctrl-a/e '(t   ;For C-a. Possible values: nil, t, 'reverse
                                 . t)) ;For C-e. Possible values: nil, t, 'reverse
    (setq org-catch-invisible-edits 'smart)
    ;; Make firefox the default web browser for applications like viewing
    ;; an html file exported from Org ( C-c C-e h o )
    (when (executable-find "firefox")
      (add-to-list 'org-file-apps '("\\.x?html\\'" . "firefox %s")))
    (setq org-return-follows-link t)
    (setq org-completion-use-ido t)
    (setq org-log-done t)))

(provide 'setup-org)
;;; setup-org.el ends here
