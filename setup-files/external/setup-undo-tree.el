;;; setup-undo-tree.el ---                           -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Jacob Faibussowitsch

;; Author: Jacob Faibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; `undo-tree' package also remaps `undo' and `undo-only' to `undo-tree-undo'
;;
;; * `undo-tree-visualize' bindings
;;
;; | [remap previous-line] | undo-tree-visualize-undo |
;; | [remap next-line]     | undo-tree-visualize-redo |
;; | C-p                   | undo-tree-visualize-undo |
;; | C-n                   | undo-tree-visualize-redo |
;;
;; Horizontal motion keys switch branch
;; | [remap forward-char]  | undo-tree-visualize-switch-branch-right |
;; | [remap backward-char] | undo-tree-visualize-switch-branch-left  |
;; | C-f                   | undo-tree-visualize-switch-branch-right |
;; | C-b                   | undo-tree-visualize-switch-branch-left  |
;;
;; Paragraph motion keys undo/redo to significant points in tree
;; | [remap backward-paragraph] | undo-tree-visualize-undo-to-x |
;; | [remap forward-paragraph]  | undo-tree-visualize-redo-to-x |
;; | M-{                        | undo-tree-visualize-undo-to-x |
;; | M-}                        | undo-tree-visualize-redo-to-x |
;; | C-up                       | undo-tree-visualize-undo-to-x |
;; | C-down                     | undo-tree-visualize-redo-to-x |
;;
;; Mouse sets buffer state to node at click
;; | [mouse-1] | undo-tree-visualizer-mouse-set |
;;
;; Toggle timestamps
;; | t | undo-tree-visualizer-toggle-timestamps |
;;
;; Toggle diff
;; | d | undo-tree-visualizer-toggle-diff |
;;
;; Toggle selection mode
;; | s | undo-tree-visualizer-selection-mode |
;;
;; Horizontal scrolling may be needed if the tree is very wide
;; | , / < | undo-tree-visualizer-scroll-left  |
;; | . / > | undo-tree-visualizer-scroll-right |
;;
;; Vertical scrolling may be needed if the tree is very tall
;; | Page Down | undo-tree-visualizer-scroll-up   |
;; | Page Up   | undo-tree-visualizer-scroll-down |
;;
;; Quit/abort visualizer
;; | q   | undo-tree-visualizer-quit  |
;; | C-q | undo-tree-visualizer-abort |

(use-package-select undo-tree
  :diminish
  :commands (undo-tree-visualize undo-tree-undo undo-tree-redo)
  :bind
  ("C-_"   . 'undo-tree-undo)
  ("C-x u" . 'undo-tree-visualize)
  ("M-_"   . 'undo-tree-redo)
  :config
  (jf/defadvice +undo--append-compression-extension-to-file-name (file)
    :filter-return #'undo-tree-make-history-save-file-name
    (concat file (if (executable-find "zstd")
		     ".zst"
		   ".gz")))
  (jf/defadvice +undo--undo-tree-save-history-quiet (func &rest args)
    :around #'undo-tree-save-history
    (let ((message-log-max nil)
          (inhibit-message t))
      (apply func args)))

  (global-undo-tree-mode)
  :custom
  (undo-tree-enable-undo-in-region          nil)
  (undo-tree-visualizer-timestamps          t)
  (undo-tree-visualizer-relative-timestamps nil)
  (undo-tree-auto-save-history              t)
  (undo-tree-history-directory-alist       `(("." . ,(concat user-cache-dir "undo-tree-hist/"))))
  :custom-face
  (undo-tree-visualizer-active-branch-face ((t (:foreground "brightgreen" :weight bold)))))

(provide 'setup-undo-tree)
;;; setup-undo-tree.el ends here
