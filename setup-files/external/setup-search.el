;;; setup-search.el ---                              -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package-select deadgrep
  :if (executable-find "rg")
  :bind
  ("C-c s" . deadgrep))

(use-package-select visual-regexp :defer)

(use-package-select visual-regexp-steroids
  ;;:disabled t
  :defer-incrementally visual-regexp
  :bind
  (("C-c r" . vr/replace      )
   ("C-c q" . vr/query-replace))
  :config
  (setq vr/command-python (replace-regexp-in-string (rx bos "python ") "python3 " vr/command-python)))

(>=e "28"
    (use-package-select xref
      :ensure nil ;; built-in
      :defer t
      :custom
      (xref-search-program (if (executable-find "rg") 'ripgrep 'grep))))

(provide 'setup-search)
;;; setup-search.el ends here
