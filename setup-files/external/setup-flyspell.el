;;; setup-flyspell.el ---                            -*- lexical-binding: t; -*-
;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MacBook-Pro.local>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;
(use-package-select flyspell
  :ensure nil ;; built-in
  :defer-incrementally ispell
  ;;:after ispell
  :hook
  (text-mode . flyspell-mode)
  (prog-mode . flyspell-prog-mode)
  :config
  (add-hook
   'flyspell-mode-hook
   (defun +spell-inhibit-duplicate-detection-maybe-h ()
     "Don't mark duplicates when style/grammar linters are present. e.g. proselint and langtool."
     (and (or (and (bound-and-true-p flycheck-mode)
		   (executable-find "proselint"))
	      (featurep 'langtool))
	  (setq-local flyspell-mark-duplications-flag nil))))
  :custom
  (flyspell-issue-welcome-flag nil)
  ;; Significantly speeds up flyspell, which would otherwise print messages for every word
  ;; when checking the entire buffer
  (flyspell-issue-message-flag nil)
  (flyspell-delay              2)
  (ispell-list-command         "--list")
  (ispell-silently-savep       t))

(provide 'setup-flyspell)
;;; setup-flyspell.el ends here
