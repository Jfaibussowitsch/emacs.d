;;; setup-completion.el ---                       -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Jacob Faibussowitsch

;; Author: Jacob Faibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; (use-package-select fancy-dabbrev
;;   :commands (fancy-dabbrev-mode)
;;   :diminish
;;   :hook
;;   (prog-mode . fancy-dabbrev-mode)
;;   :bind
;;   (("TAB". 'fancy-dabbrev-expand-or-indent)
;;    ("<backtab>" . 'fancy-dabbrev-backward))
;;   :custom
;;   (dabbrev-case-distinction nil)
;;   (dabbrev-case-fold-search t)
;;   (dabbrev-case-replace nil)
;;   (fancy-dabbrev-preview-delay 0.2)
;;   (fancy-dabbrev-expansion-on-preview-only t)
;;   (fancy-dabbrev-preview-context 'before-non-word))

(use-package-select ivy
  :diminish
  :bind
  (("C-c C-r" . ivy-resume)
   ("C-x b"   . ivy-switch-buffer-other-window))
  :hook jf/before-first-minibuffer
  :config
  (ivy-mode)
  :custom
  (ivy-count-format        "(%d/%d) ")
  (ivy-use-virtual-buffers t))

(use-package-select ivy-hydra
  :after (ivy hydra))

(use-package-select swiper
  :defer-incrementally ivy
  :commands (swiper swiper-backward)
  :diminish
  :bind
  (("C-s" . swiper-isearch)
   ("C-r" . swiper-isearch-backward)))

(use-package-select counsel
  :diminish
  :defer-incrementally ivy swiper
  :bind ("M-x" . counsel-M-x)
  :config
  (counsel-mode))

(use-package-select auto-complete-config
  :ensure auto-complete
  :commands (ac-next ac-previous)
  :diminish
  :bind (:map ac-completing-map
              ("\r"   . nil)
              ([down] . nil)
              ([up]   . nil))
  :hook (prog-mode . ac-config-default)
  :config
  (ac-flyspell-workaround)
  ;; (define-key ac-completing-map [down] nil)
  ;; (define-key ac-completing-map [up]   nil)
  :custom
  (ac-auto-start                             3)
  (ac-auto-show-menu                         nil)
  (ac-delay                                  0.2)
  (ac-use-menu-map                           t)
  (ac-show-menu-immediately-on-auto-complete nil)
  (ac-candidate-limit                        4)
  (ac-candidate-menu-min                     5)
  (ac-comphist-file                          (concat user-cache-dir "ac-comphist.cache")))

(provide 'setup-completion)
;;; setup-completion.el ends here
