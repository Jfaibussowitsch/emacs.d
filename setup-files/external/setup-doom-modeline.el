;;; setup-doom-modeline.el ---                       -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(use-package-select all-the-icons
  :defer t)

(use-package-select doom-modeline
  :hook (terminal-init-xterm . doom-modeline-mode)
  :config
  (require 'all-the-icons)
  (column-number-mode 1)
  :custom
  ;;(doom-modeline-unicode-fallback t)
  (doom-modeline-icon                     t)
  (doom-modeline-major-mode-icon          t)
  (doom-modeline-major-mode-color-icon    t)
  (doom-modeline-buffer-state-icon        t)
  (doom-modeline-buffer-modification-icon t)
  (doom-modeline-modal-icon               t)
  (doom-modeline-env-version              t)
  (doom-modeline-minor-modes              nil)
  (doom-modeline-indent-info              nil)
  (doom-modeline-lsp                      t)
  ;;(doom-modeline-project-detection       'project)
  (doom-modeline-project-detection       'auto)
  (doom-modeline-buffer-encoding          nil)
  (doom-modeline-irc                      nil)
  (doom-modeline-buffer-file-name-style  'relative-from-project)
  :custom-face
  (doom-modeline-lsp-success ((t (:inherit success :weight normal)))))

(provide 'setup-doom-modeline)
;;; setup-doom-modeline.el ends here
