;;; setup-filladapt.el ---                           -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Jacob Faibussowitsch

;; Author: Jacob Faibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(use-package-select filladapt
  :diminish
  :hook
  ((prog-mode     . auto-fill-mode)
   (text-mode     . (lambda () (auto-fill-mode -1)))
   (prog-mode     . filladapt-mode)
   (c-mode-common . (lambda () (when (featurep 'filladapt) (c-setup-filladapt)))))
  :custom
  (fill-column                     90)
  (auto-fill-function              'do-auto-fill)
  (comment-auto-fill-only-comments t))

(provide 'setup-filladapt)
;;; setup-filladapt.el ends here
