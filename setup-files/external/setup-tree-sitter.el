;;; setup-tree-sitter.el ---                         -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Jacob Faibussowitsch

;; Author: Jacob Faibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(defconst jf/enable-builtin-treesitter-p
  (and
   (>=e "29" t)
   (string= (getenv "BUILTIN_TREESITTER") "1"))
  "Enable or disable builtin treesitter")

(use-package-select treesit
  :ensure nil ;; builtin
  :if (and jf/enable-builtin-treesitter-p (treesit-available-p))
  :init
  (add-to-list 'treesit-extra-load-path user-treesit-modules-dir)
  :custom
  (treesit-font-lock-level 4))

(use-package-select c-ts-mode
  :ensure nil ;; builtin
  :after  treesit
  :defer  t
  :hook ((c-ts-mode . jf/setup-c-ts-mode)
         (c++-ts-mode . jf/setup-c++-ts-mode))
  :init
  (let ((treesit-have-c-p   (treesit-language-available-p 'c))
        (treesit-have-cpp-p (treesit-language-available-p 'cpp)))
    (when treesit-have-c-p
      (add-to-list 'major-mode-remap-alist '(c-mode . c-ts-mode)))
    (when treesit-have-cpp-p
      (add-to-list 'major-mode-remap-alist '(c++-mode . c++-ts-mode)))
    (when (or treesit-have-c-p treesit-have-cpp-p)
      (add-to-list 'major-mode-remap-alist '(c-or-c++-mode . c-or-c++-ts-mode))))
  :config
  (defun jf/c-ts-mode-font-lock-rules (mode)
    (treesit-font-lock-rules
     :default-language mode

     :override t
     :feature 'constant
     `(;; values in ALL CAPS should be considered constants...
       ((identifier) @name @font-lock-constant-face
        (:match ,(rx bos (+ (any "A-Z_")) eos) @name)))

     :override t
     :feature 'type
     `(,@(when (eq mode 'cpp)
           '((namespace_definition
              name: (namespace_identifier) @font-lock-constant-face)
             (nested_namespace_specifier
              (namespace_identifier) :+ @font-lock-type-face)
             ;; class function declaration should highlight the class name as a type
             ;; ((qualified_identifier
             ;;   scope: (namespace_identifier) @ns-ident @font-lock-type-face
             ;;   (:match ,(rx bos (+ (any alpha)) eos) @ns-ident)))
             (qualified_identifier
              scope: (namespace_identifier) @font-lock-type-face)
             ;; auto is a type
             (auto) @font-lock-type-face
             ;; using namespace foo::bar should highlight bar with constant face like all other
             ;; namespaces
             (using_declaration "using" "namespace"
                                (qualified_identifier
                                 name: (identifier) @font-lock-constant-face))
             (using_declaration "using" "namespace" (identifier) @font-lock-constant-face))))

     :override t
     :feature 'function
     `(,@(when (eq mode 'cpp)
           '(;; namespace-qualified function calls should still be treated as function calls
             (call_expression
              function: (qualified_identifier name: (_) @font-lock-function-call-face))
             ;; template function calls are... also function calls!
             (template_function
              name: (identifier) @font-lock-function-call-face)
             ;; Previous version that included the the call_expression node as well
             ;; ((call_expression (template_function
             ;;                    name: [(identifier) @font-lock-function-call-face
             ;;                           (qualified_identifier name: (_) @font-lock-function-call-face)])))
             ;; template method calls are... also function calls!
             (template_method
              name: (field_identifier) @font-lock-function-call-face))))

     :override t
     :feature 'keyword
     `(,@(when (eq mode 'cpp)
           '((this) @font-lock-builtin-face))
       (true) @font-lock-builtin-face
       (false) @font-lock-builtin-face
       ("...") @font-lock-operator-face)))

  (defun jf/setup-cc-ts-mode-base (mode)
    (dolist (item (jf/c-ts-mode-font-lock-rules mode))
      (add-to-list 'treesit-font-lock-settings item))
    (treesit-major-mode-setup))

  (defun jf/setup-c-ts-mode ()
    (jf/setup-cc-ts-mode-base 'c))

  (defun jf/setup-c++-ts-mode ()
    (jf/setup-cc-ts-mode-base 'cpp))
  :custom
  (c-ts-mode-enable-doxygen t))

;; tree-sitter
;; TODO: eventually migrate to builtin version
(use-package-select tree-sitter-langs
  :unless jf/enable-builtin-treesitter-p
  :diminish
  :defer t)

(use-package-select tree-sitter
  :unless jf/enable-builtin-treesitter-p
  :diminish
  :defer-incrementally t
  :hook (c-mode-common python-mode sh-mode rustic-mode)
  :custom
  (tree-sitter-debug-jump-buttons t)
  (tree-sitter-debug-highlight-jump-region t))

(use-package-select tree-sitter-hl
  :unless jf/enable-builtin-treesitter-p
  :ensure tree-sitter
  :after (:all tree-sitter tree-sitter-langs)
  :diminish
  :hook tree-sitter-after-on
  :init
  (add-to-list 'tree-sitter-major-mode-language-alist '(cython-mode . python))
  :config
  (defmacro cuda-identifier (name)
    `[[((type_identifier) @keyword
        (identifier) @type
        (ERROR (identifier) @variable)
        (.eq? @keyword ,name))]
      [((type_identifier) @keyword
        (ERROR (identifier) @type)
        (.eq? @keyword ,name))]])

  (defmacro mapper (args)
    `(mapcar (lambda (x) `(cuda-identifier ,x)) ,args))

  (tree-sitter-hl-add-patterns 'cpp
    (vconcat
     (mapcar 'macroexpand-all
             (vconcat
              (mapper
               '("__global__" "__device__" "__shared__" "__host__" "__device__" "__force inline__"
                 "__constant__"))))))
  :custom-face
  (tree-sitter-hl-face:comment            ((t (:inherit font-lock-comment-face :slant italic))))
  (tree-sitter-hl-face:constant           ((t (:inherit font-lock-constant-face))))
  (tree-sitter-hl-face:function.call      ((t (:inherit font-lock-function-call-face))))
  (tree-sitter-hl-face:function.macro     ((t (:inherit font-lock-preprocessor-face))))
  (tree-sitter-hl-face:function.special   ((t (:inherit tree-sitter-hl-face:function.macro))))
  (tree-sitter-hl-face:method.call        ((t (:inherit tree-sitter-hl-face:function.call))))
  (tree-sitter-hl-face:operator           ((t (:inherit tree-sitter-hl-face:keyword))))
  (tree-sitter-hl-face:property           ((t (:inherit font-lock-property-use-face :slant normal))))
  (tree-sitter-hl-face:type.builtin       ((t (:inherit tree-sitter-hl-face:type))))
  (tree-sitter-hl-face:variable           ((t (:inherit unspecified))))
  (tree-sitter-hl-face:variable.builtin   ((t (:inherit font-lock-builtin-face))))
  (tree-sitter-hl-face:variable.parameter ((t (:inherit tree-sitter-hl-face:variable))))
  (tree-sitter-hl-face:variable.special   ((t (:inherit tree-sitter-hl-face:variable)))))

(provide 'setup-tree-sitter)
;;; setup-tree-sitter.el ends here
