;;; setup-flycheck.el ---                            -*- lexical-binding: t; -*-
;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MacBook-Pro.local>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;
(use-package-select flycheck
  :diminish
  :defer t
  :hook  (elisp-mode c-mode-common)
  :config
  (jf/defadvice flycheck-next-error--expand-after-goto-tag (&rest _)
    "Hideshow-expand for (flycheck-next-error)."
    :after '(flycheck-next-error)
    (save-excursion (hs-show-block)))

  (jf/defadvice flycheck-previous-error--expand-after-goto-tag (&rest _)
    "Hideshow-expand for (flycheck-previous-error)."
    :after '(flycheck-previous-error)
    (save-excursion (hs-show-block)))

  (jf/defadvice flycheck-error-list-error-list-goto-error--expand-after-goto-tag (&rest _)
    "Hideshow-expand for (flycheck-error-list-goto-error)."
    :after '(flycheck-error-list-goto-error)
    (save-excursion (hs-show-block)))

  (jf/defadvice flycheck-next-error--expand-after-goto-tag (&rest _)
    "Hideshow-expand for (flycheck-next-error)."
    :after '(flycheck-next-error)
    (save-excursion (hs-show-block)))

  (jf/defadvice goto-error--expand-after-goto-tag (&rest _)
    "Hideshow-expand for (goto-err)."
    :after '(goto-error)
    (save-excursion (hs-show-block)))

  (jf/defadvice next-error--expand-after-next-tag (&rest _)
    "Hideshow-expand for (next-err)."
    :after '(next-error)
    (save-excursion (hs-show-block)))

  (jf/defadvice previous-error--expand-after-previous-tag (&rest _)
    "Hideshow-expand for (previous-err)."
    :after '(previous-error)
    (save-excursion (hs-show-block)))
  :custom
  (flycheck-disabled-checkers          '(emacs-lisp-checkdoc))
  (flycheck-check-syntax-automatically '(save idle-change mode-enabled idle-buffer-switch))
  ;; default is ("all" "extra") which correspond to -Wall and -Wextra. -Wextra is pretty
  ;; unnecessary
  (flycheck-clang-warnings '("all"))
  :custom-face
  (flycheck-error   ((t (:foreground "Red1" :weight bold))))
  (flycheck-warning ((t (:foreground "DarkOrange" :weight bold)))))

(provide 'setup-flycheck)
;;; setup-flycheck.el ends here
