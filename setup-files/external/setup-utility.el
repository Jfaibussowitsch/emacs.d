;;; setup-utility.el ---                       -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Jacob Faibussowitsch

;; Author: Jacob Faibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(use-package-select f
  :defer t)

(use-package-select dash
  :defer t
  :config
  (dash-enable-font-lock))

(use-package-select s
  :defer t)

(use-package-select auto-compile
  :defer-incrementally f dash
  :config
  (auto-compile-on-load-mode)
  (auto-compile-on-save-mode))

(use-package-select async
  :defer    t
  :commands (async-start async-inject-variables))

(use-package util
  :ensure nil ;; /misc/elisp/compiled/util.el
  :init
  (defun jf/async-compile-directory (&optional directory)
    "asynchronously byte-compile a directory (defaults to user elisp-directory)"
    (interactive)
    (require 'async)
    (or directory (setq directory user-elisp-compiled-dir))
    (async-start
     `(lambda ()
        (require 'bytecomp)
        ,(async-inject-variables "\\`\\(?:load-path\\'\\|byte-\\)")
        (let (default-directory (file-name-as-directory ,directory))
          (add-to-list 'load-path default-directory)
          (byte-recompile-directory ,directory 0 nil)))

     `(lambda (result)
	(message "end   async compile directory %s" ,directory)))
    (message    "begin async compile directory %s" directory))
  :hook
  (window-setup . (lambda () (run-with-idle-timer 5 nil #'jf/async-compile-directory))))

(use-package header2
  :ensure   nil ;; /misc/elisp/compiled/header2.el
  :commands auto-make-header
  :config
  (defun python-template ()
    "insert bare python essentials"
    (insert "#!/usr/bin/env python3\n")
    (insert "\"\"\"\n")
    (header-creation-date)
    (header-author)
    (insert "\"\"\"\n")
    (insert "from __future__ import annotations\n")
    (insert "\n")
    (insert "\n")
    (insert "def main() -> None:\n    return\n\n")
    (insert "if __name__ == \"__main__\":\n    main()\n"))

  (defun header-guard-from-file-name ()
    "Insert #ifndef FILENAME_EXT line, using buffer's file name."
    (require 'dash)
    (when (-contains? '("h" "hpp" "H" "inc" "hxx") (file-name-extension (buffer-file-name)))
      (let ((guard (upcase
		    (concat
		     (file-name-base (buffer-file-name))
		     "_"
		     (file-name-extension (buffer-file-name))))))
	(insert "#ifndef " guard "\n")
	(insert "#define " guard "\n\n\n")
	(insert "#endif // " guard "\n"))))
  :hook
  ((c-mode c++-mode) . (lambda ()
			 (setq make-header-hook '(header-guard-from-file-name))
			 (auto-make-header)))
  (python-mode . (lambda ()
		   (setq make-header-hook '(python-template))
		   (auto-make-header))))

(provide 'setup-utility)
;;; setup-utility.el ends here
