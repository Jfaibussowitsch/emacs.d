;;; setup-bindings.el ---                            -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(require 'align)
(require 'window)
(require 'bind-key)
(require 'thingatpt)

(unbind-key "C-x C-d") ;; list-directory
(unbind-key "C-z")     ;; background emacs

(bind-key "M-n" (lambda () (interactive) (next-line 10)))
(bind-key "M-p" (lambda () (interactive) (previous-line 10)))
(bind-key "C-c C-f" `ff-find-other-file)

(bind-key "C-c a" 'align)
(bind-key "M-o"   'other-window)

(defun jf/capitalize-word ()
  "Capitalize a word from any point in the word"
  (interactive)
  (pcase-let ((`(,thing-begin . ,thing-end) (bounds-of-thing-at-point 'word)))
    (capitalize-region thing-begin thing-end)))

(bind-key "M-c" #'jf/capitalize-word)

(provide 'setup-bindings)
;;; setup-bindings.el ends here
