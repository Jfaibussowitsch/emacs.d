;;; setup-dashboard.el ---                            -*- lexical-binding: t; -*-
;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MacBook-Pro.local>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

(use-package-select dashboard
  :defer-incrementally all-the-icons projectile counsel
  :init
  (dashboard-setup-startup-hook)
  :config
  (when (daemonp)
    (setq initial-buffer-choice (lambda () (switch-to-buffer dashboard-buffer-name))))
  (setq dashboard-items '((recents   . 5)
			  (projects  . 5)
                          (bookmarks . 5)
                          (registers . 5)))
  :custom
  (dashboard-set-heading-icons         t)
  (dashboard-set-file-icons            t)
  (dashboard-set-navigator             t)
  (dashboard-set-init-info             t)
  (dashboard-projects-switch-function 'counsel-projectile-switch-project-by-name))

(provide 'setup-dashboard)
;;; setup-builtin.el ends here
