;;; setup-movelines.el ---                           -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords: abbrev

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(use-package-select move-lines
  :ensure nil ;; misc/elisp/move-lines.el
  :after (:all hydra python)
  :config
  (defun jf/shift-left (start end &optional count)
    "Shift region left and activate hydra."
    (interactive
     (if mark-active
         (list (region-beginning) (region-end) current-prefix-arg)
       (list (line-beginning-position) (line-end-position) current-prefix-arg)))
    (python-indent-shift-left start end count)
    (jf/hydra-move-lines/body))

  (defun jf/shift-right (start end &optional count)
    "Shift region right and activate hydra."
    (interactive
     (if mark-active
         (list (region-beginning) (region-end) current-prefix-arg)
       (list (line-beginning-position) (line-end-position) current-prefix-arg)))
    (python-indent-shift-right start end count)
    (jf/hydra-move-lines/body))

  (defun jf/move-lines-p ()
    "Move lines up once and activate hydra."
    (interactive)
    (move-lines-up 1)
    (jf/hydra-move-lines/body))

  (defun jf/move-lines-n ()
    "Move lines down once and activate hydra."
    (interactive)
    (move-lines-down 1)
    (jf/hydra-move-lines/body))

  (defhydra jf/hydra-move-lines ()
    "Move one or multiple lines"
    ("n" move-lines-down "down")
    ("<down>" move-lines-down "down")
    ("p" move-lines-up "up")
    ("<up>" move-lines-up "up")
    ("<" python-indent-shift-left "left")
    ("<left>" python-indent-shift-left "left")
    (">" python-indent-shift-right "right")
    ("<right>" python-indent-shift-right "right"))
  :bind
  (("C-c <down>" . #'jf/move-lines-n)
   ("C-c <up>" . #'jf/move-lines-p)
   :map python-mode-map
   ("C-c <left>" . #'jf/shift-left)
   ("C-c <right>" . #'jf/shift-right)))

(provide 'setup-movelines)
;;; setup-movelines.el ends here
