;;; setup-tool-bar.el ---                            -*- lexical-binding: t; -*-
;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MacBook-Pro.local>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; disable tool, menu, and scrollbars. Emacs is designed to be keyboard-centric,
;; so these are just clutter (the scrollbar also impacts performance). Whats
;; more, the menu bar exposes functionality that emacs doesn't endorse.
;;
;; I am intentionally avoid using `menu-bar-mode', `tool-bar-mode', and
;; `scroll-bar-mode' because they do extra and unnecessary work that can be more
;; concisely, and efficiently, expressed with these six lines:
;; (push '(menu-bar-lines . 0)   default-frame-alist)
;; (push '(tool-bar-lines . 0)   default-frame-alist)
;; (push '(vertical-scroll-bars) default-frame-alist)

;; These are disabled directly through their frame parameters to avoid the extra
;; work their minor modes do, but their variables must be unset too, otherwise
;; users will have to cycle them twice to re-enable them.
;; (setq
;;  menu-bar-mode   nil
;;  tool-bar-mode   nil
;;  scroll-bar-mode nil)
(use-package-select tool-bar
  :ensure nil
  :preface
  (push '(tool-bar-lines . 0) default-frame-alist)
  (setq tool-bar-mode nil))

(use-package-select scroll-bar
  :ensure nil
  :preface
  (push '(vertical-scroll-bars) default-frame-alist)
  (setq scroll-bar-mode nil))

(use-package-select menu-bar
  :ensure nil
  :preface
  (push '(menu-bar-lines . 0) default-frame-alist)
  (setq menu-bar-mode nil))

(provide 'setup-tool-bar)
;;; setup-tool-bar.el ends here
