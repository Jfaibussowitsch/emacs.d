;;; setup-hideshow.el ---                            -*- lexical-binding: t; -*-
;; Copyright (C) 2022  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MacBook-Pro.local>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;
(defun jf/hideshow-toggle (&rest ignored)
  "Toggle hide-show of current region."
  (interactive)
  (end-of-line)
  (if (save-excursion
        (end-of-line 1)
        (or (hs-already-hidden-p)
            (progn
              (forward-char 1)
              (hs-already-hidden-p))))
      (hs-show-block)
    (hs-hide-block)
    (beginning-of-line)))

(defun jf/toggle-fold ()
  "Toggle folding in a code-block"
  (interactive)
  (save-excursion
    (beginning-of-line)
    (hs-toggle-hiding)))

(defun jf/hide-all ()
  "Hide all code blocks in a file"
  (interactive)
  (condition-case-unless-debug nil
      (hs-hide-level 1)
    (error nil)))

(defun jf/hs-show (&rest _)
  (save-excursion (hs-show-block)))

(use-package-select hideshow
  :ensure nil ;; built-in
  :commands (hs-toggle-hiding
             hs-hide-block
             hs-hide-level
             hs-show-all
             hs-hide-all)
  :bind
  (("C-c TAB" . #'jf/hideshow-toggle)
   ("C-c h"   . #'hs-hide-level))
  :hook
  (prog-mode . jf/hide-all)
  :diminish
  hs-minor-mode
  :config
  (advice-add 'goto-line :after #'jf/hs-show)
  (jf/defadvice +fold--hideshow-ensure-mode-a (&rest _)
    "Ensure `hs-minor-mode' is enabled when we need it, no sooner or later."
    :before '(hs-toggle-hiding hs-hide-block hs-hide-level hs-show-all hs-hide-all)
    (unless (bound-and-true-p hs-minor-mode)
      (hs-minor-mode +1))))

(provide 'setup-hideshow)
;;; setup-hideshow.el ends here
