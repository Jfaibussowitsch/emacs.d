;;; setup-smerge-mode.el ---                            -*- lexical-binding: t; -*-
;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MacBook-Pro.local>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

(use-package-select smerge-mode
  :ensure nil ;; built-in
  :defer t
  :init
  (setq smerge-command-prefix "\C-cv")
  :config
  (jf/after hideshow
    (advice-add 'smerge-next :after #'jf/hs-show)
    (advice-add 'smerge-prev :after #'jf/hs-show)))

(provide 'setup-smerge-mode)
;;; setup-smerge-mode.el ends here
