;;; setup-hl-line+.el ---                            -*- lexical-binding: t; -*-
;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MacBook-Pro.local>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;
(use-package-select hl-line+
  :ensure nil ;; misc/elisp/compiled/hl-line+
  :diminish
  :defer-incrementally t
  :hook
  ((prog-mode text-mode) . hl-line-mode)
  :config
  (hl-line-when-idle-interval 1)
  (toggle-hl-line-when-idle   1)
  :custom-face
  (hl-line ((t (:background "grey19" :inherit nil)))))

(provide 'setup-hl-line+)
;;; setup-hl-line+.el ends here
