;;; setup-ibuffer.el ---                             -*- lexical-binding: t; -*-

;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MBP.fios-router.home>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(use-package-select ibuffer
  :ensure nil ;; built-in
  :defer t
  :bind
  ("C-x C-b" . ibuffer)
  :config
  (setq ibuffer-show-empty-filter-groups nil
	ibuffer-filter-group-name-face '(:inherit (success bold)))
  (define-ibuffer-column size
    (:name "Size"
	   :inline t
	   :header-mouse-map ibuffer-size-header-map)
    (file-size-human-readable (buffer-size)))
  (setq ibuffer-formats
	`((mark modified read-only locked
		" " (name 20 20 :left :elide)
		" " (size 14 -1 :right)
		" " (mode 16 16 :left :elide)
		,@(when (require 'ibuffer-vc nil t)
                    '(" " (vc-status 12 :left)))
		" " filename-and-process)
	  (mark " " (name 16 -1) " " filename))))

(use-package-select ibuffer-projectile
  :defer-incrementally ibuffer projectile
  :hook
  (ibuffer . (lambda () (ibuffer-projectile-set-filter-groups)
	       (unless (eq ibuffer-sorting-mode 'alphabetic)
		 (ibuffer-do-sort-by-alphabetic)))))

(provide 'setup-ibuffer)
;;; setup-ibuffer.el ends here
