;;; setup-dired.el ---                            -*- lexical-binding: t; -*-
;; Copyright (C) 2021  jacobfaibussowitsch

;; Author: jacobfaibussowitsch <jacobfaibussowitsch@Jacobs-MacBook-Pro.local>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;
(use-package-select dired
  :ensure nil
  :commands dired-jump
  :defer t
  :custom
  ;; don't prompt to revert; just do it
  (dired-auto-revert-buffer t)
  ;; suggest a target for moving/copying intelligently
  (dired-dwim-target                       t)
  (dired-hide-details-hide-symlink-targets nil)
  ;; Always copy/delete recursively
  (dired-recursive-copies  'always)
  (dired-recursive-deletes 'top)
  ;; Ask whether destination dirs should get created when copying/removing files.
  (dired-create-destination-dirs 'ask)
  ;; Where to store image caches
  (image-dired-dir                    (concat user-cache-dir  "image-dired/"))
  (image-dired-db-file                (concat image-dired-dir "db.el"))
  (image-dired-gallery-dir            (concat image-dired-dir "gallery/"))
  (image-dired-temp-image-file        (concat image-dired-dir "temp-image"))
  (image-dired-temp-rotate-image-file (concat image-dired-dir "temp-rotate-image"))
  ;; Screens are larger nowadays, we can afford slightly larger thumbnails
  (image-dired-thumb-size 150))

(provide 'setup-dired)
;;; setup-dired.el ends here
