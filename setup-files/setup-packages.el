;;; setup-packages.el -*- no-byte-compile: t -*-
;;; Package.el
;; ----------
(jf/after package
  (>=e "25"
      (progn
        ;; If non-nil, do activities asynchronously, like refreshing menu
        (setq package-menu-async t)
        (>=e "27"
            (>=e "28" (setq package-native-compile t))
          (package-initialize))))

  (add-to-list 'package-archives '("gnu"   . "https://elpa.gnu.org/packages/") t)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/")    t)

  ;; get use-package if it doesn't exist yet
  (unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package)))

;;; Use-package
;; ------------
(jf/after use-package-core
  (>=e "26" (font-lock-remove-keywords 'emacs-lisp-mode use-package-font-lock-keywords))

  (setq use-package-always-ensure          t
	use-package-compute-statistics     t
	use-package-verbose                t
	use-package-minimum-reported-time .1)

  (dolist (keyword '(:defer-incrementally))
    (push keyword use-package-deferring-keywords)
    (setq use-package-keywords
          (use-package-list-insert keyword use-package-keywords :after)))

  (defalias 'use-package-normalize/:defer-incrementally #'use-package-normalize-symlist)
  (defun use-package-handler/:defer-incrementally (name _keyword targets rest state)
    (use-package-concat
     `((jf/load-packages-incrementally
        ',(if (equal targets '(t))
              (list name)
            (append targets (list name)))))
     (use-package-handler/:after name _keyword targets rest state)
     (use-package-process-keywords name rest state)))

  (use-package bind-key
    :ensure   nil ; built-in with use-package
    :defer    t
    :commands bind-key unbind-key))

(defmacro use-package-select (name &rest args)
  "Like `use-package', but adding package to package-selected-packages.
NAME and ARGS are as in `use-package'."
  (declare (indent defun))
  `(progn
     (add-to-list 'package-selected-packages ',name)
     (use-package ,name
       ,@args)))

;;; Deferred loading
;; -----------------
(defvar jf/incremental-packages '(t)
  "A list of packages to load incrementally after startup. Any large packages
here may cause noticeable pauses, so it's recommended you break them up into
sub-packages. For example, `org' is comprised of many packages, and can be
broken up into:
  (jf-load-packages-incrementally
   '(calendar find-func format-spec org-macs org-compat
     org-faces org-entities org-list org-pcomplete org-src
     org-footnote org-macro ob org org-clock org-agenda
     org-capture))
This is already done by the lang/org module, however.
If you want to disable incremental loading altogether, either remove
`jf/load-packages-incrementally-h' from `emacs-startup-hook' or set
`jf/incremental-first-idle-timer' to nil. Incremental loading does not occur
in daemon sessions (they are loaded immediately at startup).")

(defconst jf/incremental-first-idle-timer 2.0
  "How long (in idle seconds) until incremental loading starts.
Set this to nil to disable incremental loading.")

(defconst jf/incremental-idle-timer 0.75
  "How long (in idle seconds) in between incrementally loading packages.")

(defvar jf/incremental-load-immediately (daemonp)
  "If non-nil, load all incrementally deferred packages immediately at startup.")

;; stolen from doom
(defun jf/load-packages-incrementally (packages &optional now)
  "Registers PACKAGES to be loaded incrementally.
If NOW is non-nil, load PACKAGES incrementally, in `jf/incremental-idle-timer'
intervals."
  (if (not now)
      (jf/appendl jf/incremental-packages packages)
    (while packages
      (let* ((gc-cons-threshold most-positive-fixnum)
             (req (pop packages)))
        (unless (featurep req)
          (message "--------------- incrementally loading %s" req)
          (condition-case-unless-debug e
              (or (while-no-input
                    ;; If `default-directory' is a directory that doesn't exist
                    ;; or is unreadable, Emacs throws up file-missing errors, so
                    ;; we set it to a directory we know exists and is readable.
                    (let ((default-directory user-emacs-directory)
                          (inhibit-message t)
                          file-name-handler-alist)
                      (require req nil t))
                    t)
                  (push req packages))
            (error
             (message "xxxxxxxxxxxxxxx failed to load %S package incrementally, because: %s" req e)))
          (if (not packages)
              (message "--------------- finished incremental loading")
            (run-with-idle-timer jf/incremental-idle-timer
                                 nil #'jf/load-packages-incrementally
                                 packages t)
            (setq packages nil)))))))
(add-hook 'emacs-startup-hook #'jf/load-packages-incrementally-h)

(defun jf/load-packages-incrementally-h ()
  "Begin incrementally loading packages in `jf/incremental-packages'.
   If this is a daemon session, load them all immediately instead."
  (if jf/incremental-load-immediately
      (mapc #'require (cdr jf/incremental-packages))
    (when (numberp jf/incremental-first-idle-timer)
      (run-with-idle-timer jf/incremental-first-idle-timer
                           nil #'jf/load-packages-incrementally
                           (cdr jf/incremental-packages) t))))

(jf/defconst-load-path-directory builtin-setup-files-directory
                                 user-setup-files-dir "builtin"
                                 "Directory housing setup files for builtin packages")

(jf/defconst-load-path-directory external-setup-files-directory
                                 user-setup-files-dir "external"
                                 "Directory housing setup files for external packages")

;; autoloads package.el
(use-package package
  :ensure nil ;; built-in
  :custom
  (package-user-dir (file-name-as-directory (expand-file-name "elpa" user-emacs-directory))))

(provide 'setup-packages)
;;; setup-packages.el ends here
