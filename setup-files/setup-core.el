;; setup-core.el
;;; General setup
;; ---------------
(require 'cl-lib)

(defmacro >=e (version &rest body)
  "Emacs VERSION check wrapper around BODY.
BODY can contain both `if' block (for stuff to execute if emacs
is equal or newer than VERSION) and `else' block (for stuff to
execute if emacs is older than VERSION).  Example:
  (>=e \"25.0\"
      (defun-compatible-with-25.0)
    (defun-not-compatible-in-older-version))"
  (declare (indent 2))          ;`if'-style indentation where this macro is used
  `(if (version<= ,version emacs-version)
       ,@body))

;; The following utility functions are shamelessly stolen from doom emacs, so see doom
;; emacs for further explanation/usage
(defmacro jf/after (package &rest body)
  "Evaluate BODY after PACKAGE have loaded.
PACKAGE is a symbol or list of them. These are package names, not modes,
functions or variables. It can be:
- An unquoted package symbol (the name of a package)
    (jf/after helm BODY...)
- An unquoted list of package symbols (i.e. BODY is evaluated once both magit
  and git-gutter have loaded)
    (jf/after (magit git-gutter) BODY...)
- An unquoted, nested list of compound package lists, using any combination of
  :or/:any and :and/:all
    (jf/after (:or package-a package-b ...)  BODY...)
    (jf/after (:and package-a package-b ...) BODY...)
    (jf/after (:and package-a (:or package-b package-c) ...) BODY...)
  Without :or/:any/:and/:all, :and/:all are implied.
This is a wrapper around `eval-after-load' that:
1. Suppresses warnings for disabled packages at compile-time
2. No-ops for package that are disabled by the user (via `package!')
3. Supports compound package statements (see below)
4. Prevents eager expansion pulling in autoloaded macros all at once"
  (declare (indent defun) (debug t))
  (if (symbolp package)
      (unless (memq package (bound-and-true-p doom-disabled-packages))
        (list (if (or (not (bound-and-true-p byte-compile-current-file))
                      (require package nil 'noerror))
                  #'progn
                #'with-no-warnings)
              ;; We intentionally avoid `with-eval-after-load' to prevent eager
              ;; macro expansion from pulling (or failing to pull) in autoloaded
              ;; macros/packages.
              `(eval-after-load ',package ',(macroexp-progn body))))
    (let ((p (car package)))
      (cond ((not (keywordp p))
             `(after! (:and ,@package) ,@body))
            ((memq p '(:or :any))
             (macroexp-progn
              (cl-loop for next in (cdr package)
                       collect `(after! ,next ,@body))))
            ((memq p '(:and :all))
             (dolist (next (cdr package))
               (setq body `((after! ,next ,@body))))
             (car body))))))

(defmacro jf/appendl (symbol &rest lists)
  "Append LISTS to SYMBOL in place."
  `(setq ,symbol (append ,symbol ,@lists)))

(defmacro jf/pushnew (place &rest values)
  "Push VALUES sequentially into PLACE, if they aren't already present.
This is a variadic `cl-pushnew'."
  (let ((var (make-symbol "result")))
    `(dolist (,var (list ,@values) (with-no-warnings ,place))
       (cl-pushnew ,var ,place :test #'equal))))

(defmacro jf/letf (bindings &rest body)
  "Temporarily rebind function and macros in BODY.
Intended as a simpler version of `cl-letf' and `cl-macrolet'.
BINDINGS is either a) a list of, or a single, `defun' or `defmacro'-ish form, or
b) a list of (PLACE VALUE) bindings as `cl-letf*' would accept.
TYPE is either `defun' or `defmacro'. NAME is the name of the function. If an
original definition for NAME exists, it can be accessed as a lexical variable by
the same name, for use with `funcall' or `apply'. ARGLIST and BODY are as in
`defun'.
\(fn ((TYPE NAME ARGLIST &rest BODY) ...) BODY...)"
  (declare (indent defun))
  (setq body (macroexp-progn body))
  (when (memq (car bindings) '(defun defmacro))
    (setq bindings (list bindings)))
  (dolist (binding (reverse bindings) (macroexpand body))
    (let ((type (car binding))
          (rest (cdr binding)))
      (setq
       body (pcase type
              (`defmacro `(cl-macrolet ((,@rest)) ,body))
              (`defun `(cl-letf* ((,(car rest) (symbol-function #',(car rest)))
                                  ((symbol-function #',(car rest))
                                   (lambda ,(cadr rest) ,@(cddr rest))))
                         (ignore ,(car rest))
                         ,body))
              (_
               (when (eq (car-safe type) 'function)
                 (setq type (list 'symbol-function type)))
               (list 'cl-letf (list (cons type rest)) body)))))))

(defmacro jf/quiet (&rest forms)
  "Run FORMS without generating any output.
This silences calls to `message', `load', `write-region' and anything that
writes to `standard-output'. In interactive sessions this won't suppress writing
to *Messages*, only inhibit output in the echo area."
  `(jf/letf ((standard-output (lambda (&rest _)))
             (defun message (&rest _))
             (defun load (file &optional noerror nomessage nosuffix must-suffix)
               (funcall load file noerror t nosuffix must-suffix))
             (defun write-region (start end filename &optional append visit lockname mustbenew)
               (unless visit (setq visit 'no-message))
               (funcall write-region start end filename append visit lockname mustbenew)))
	    ,@forms))

(defun jf/silence (fn &rest args)
  "asdasdasd"
  (jf/quiet (apply fn args)))

(defun jf/enlist (exp)
  "Return EXP wrapped in a list, or as-is if already a list."
  (declare (pure t) (side-effect-free t))
  (if (listp exp) exp (list exp)))

(defmacro jf/defadvice (symbol arglist &optional docstring &rest body)
  "Define an advice called SYMBOL and add it to PLACES.
ARGLIST is as in `defun'. WHERE is a keyword as passed to `advice-add', and
PLACE is the function to which to add the advice, like in `advice-add'.
DOCSTRING and BODY are as in `defun'.
\(fn SYMBOL ARGLIST &optional DOCSTRING &rest [WHERE PLACES...] BODY\)"
  (declare (doc-string 3) (indent defun))
  (unless (stringp docstring)
    (push docstring body)
    (setq docstring nil))
  (let (where-alist)
    (while (keywordp (car body))
      (push `(cons ,(pop body) (jf/enlist ,(pop body)))
            where-alist))
    `(progn
       (defun ,symbol ,arglist ,docstring ,@body)
       (dolist (targets (list ,@(nreverse where-alist)))
         (dolist (target (cdr targets))
           (advice-add target (car targets) #',symbol))))))

;; don't truncate eval expression output
(setq eval-expression-print-level  nil
      eval-expression-print-length nil)

;; dont echo startup messages unless daemon
(unless (daemonp)
  (advice-add #'display-startup-echo-area-message :override #'ignore))

;; setup default logging locations
(setq async-byte-compile-log-file  (concat user-temp-file-dir "async-bytecomp.log")
      ido-save-directory-list-file (concat user-cache-dir     "ido.cache")
      auto-save-list-file-prefix    user-cache-dir)

;; accept 'y' in lieu of yes
(defalias 'yes-or-no-p 'y-or-n-p)

;; Unicode
(set-language-environment "UTF-8")
(set-charset-priority     'unicode)

(when (string= system-type "darwin")
  (jf/after auth-source
    (jf/pushnew auth-sources 'macos-keychain-internet 'macos-keychain-generic))
  ;; use spotlight for search
  (setq locate-command "mdfind")
  ;; on macOS ls doesn't support the --dired option while on Linux it is supported
  (setq dired-use-ls-dired nil))

;; Setup backup files
(setq backup-directory-alist         `(("." . ,user-backup-dir  ))
      auto-save-file-name-transforms `((".*"  ,user-backup-dir t)))

(setq tramp-backup-directory-alist backup-directory-alist
      ;; don't make backups for version controlled files
      vc-make-backup-files nil
      create-lockfiles     nil
      ;; disables making backups entirely, if you have a good "undo" package with
      ;; persistent undo history, then backups are relatively pointless
      make-backup-files    nil
      ;; But in case the user does enable it, some sensible defaults:
      ;; number each backup file
      version-control     t
      ;; instead of renaming current file (clobbers links)
      backup-by-copying   t
      ;; clean up after itself
      delete-old-versions t
      kept-old-versions   5
      kept-new-versions   5)

;; Try to keep the cursor out of the read-only portions of the minibuffer.
(setq minibuffer-prompt-properties
      '(read-only t intangible t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

;;; Customization
;; --------------

(setq-default indent-tabs-mode nil) ; should indent not tab
(setq show-trailing-whitespace  t   ; highlight white-space
      require-final-newline     t   ; always have final newline
      auto-window-vscroll       nil
      display-line-numbers-type nil ; makes scroll laggy
      ;; Cull duplicates in the kill ring to reduce bloat and make the kill ring
      ;; easier to peruse (with `counsel-yank-pop' or `helm-show-kill-ring'.
      kill-do-not-save-duplicates t
      blink-matching-paren        t   ; if t, highlight the paren
      case-fold-search            t   ; case sensitive search
      flymake-mode                nil ; disable flymake
      ;; Show current key-sequence in minibuffer ala 'set showcmd' in vim. Any
      ;; feedback after typing is better UX than no feedback at all.
      echo-keystrokes                   0.02
      byte-compile-docstring-max-column 4000
      ;; make inferior shells pick up on aliases
      ;;shell-command-switch              "-ic"
      ;; Non-nil means visiting a file uses its truename as the visited-file name. That
      ;; is, the buffer visiting the file has the truename as the value of
      ;; ‘buffer-file-name’. The truename of a file is found by chasing all links both at
      ;; the file level and at the levels of the containing directories.
      find-file-visit-truename t)

(global-so-long-mode 1) ; avoid performance issues in files with very long lines
(electric-pair-mode  1) ; insert brackets

(put 'upcase-region   'disabled nil)
(put 'downcase-region 'disabled nil)

(defun jf/join-kill-line (&rest _)
  (when (and (eolp) (not (bolp)))
    (save-excursion
      (forward-char 1)
      (just-one-space 0))))

(advice-add 'kill-line :before #'jf/join-kill-line)

;; ElDoc
(add-hook 'eval-expression-minibuffer-setup-hook #'eldoc-mode)

(defadvice server-visit-files (before parse-numbers-in-lines (files proc &optional nowait) activate compile)
  "looks for filenames like file:line or file:line:position and reparses name in such manner that position in file"
  (ad-set-arg
   0 (mapcar
      (lambda (fn)
        (let ((name (car fn)))
          (if (string-match "^\\(.*?\\):\\([0-9]+\\)\\(?::\\([0-9]+\\)\\)?$" name)
              (cons
               (match-string 1 name)
               (cons
                (string-to-number (match-string 2 name))
                (string-to-number (or (match-string 3 name) ""))))
            fn)
          ))
      files)))

;;; Optimizations
;; --------------

;; set native-comp compiler flags
(>=e "28"
    (setq native-comp-async-jobs-number 6
          native-comp-speed             2))

;; Disable bidirectional text scanning for a modest performance boost. I've set
;; this to `nil' in the past, but the `bidi-display-reordering's docs say that
;; is an undefined state and suggest this to be just as good:
(setq-default bidi-display-reordering 'left-to-right
              bidi-paragraph-direction 'left-to-right)

;; Disabling the BPA makes redisplay faster, but might produce incorrect display
;; reordering of bidirectional text with embedded parentheses and other bracket
;; characters whose 'paired-bracket' Unicode property is non-nil.
(setq bidi-inhibit-bpa t)  ; Emacs 27 only

;; Reduce rendering/line scan work for Emacs by not rendering cursors or regions
;; in non-focused windows.
(setq-default cursor-in-non-selected-windows nil)

(setq
 highlight-nonselected-windows     nil
 inhibit-startup-message           t
 inhibit-startup-echo-area-message user-login-name
 inhibit-default-init              t
 ;; Shave seconds off startup time by starting the scratch buffer in
 ;; `fundamental-mode', rather than, say, `org-mode' or `text-mode', which
 ;; pull in a ton of packages. `doom/open-scratch-buffer' provides a better
 ;; scratch buffer anyway.
 ;;initial-major-mode 'fundamental-mode
 initial-scratch-message nil
 ;; Emacs "updates" its ui more often than it needs to, so slow it down slightly
 idle-update-delay       1.0 ; default is 0.5
 ;; A second, case-insensitive pass over `auto-mode-alist' is time wasted, and
 ;; indicates misconfiguration (don't rely on case insensitivity for file names)
 auto-mode-case-fold nil)

;; HACK `tty-run-terminal-initialization' is *tremendously* slow for some
;;      reason; inexplicably doubling startup time for terminal Emacs. Keeping
;;      it disabled will have nasty side-effects, so we simply delay it instead,
;;      and invoke it later, at which point it runs quickly; how mysterious!
(unless (daemonp)
  (advice-add #'tty-run-terminal-initialization :override #'ignore)
  (add-hook 'window-setup-hook
	    (lambda ()
	      (advice-remove #'tty-run-terminal-initialization #'ignore)
	      (tty-run-terminal-initialization (selected-frame) nil t))))

;; xterm with the resource ?.VT100.modifyOtherKeys: 1
;; GNU Emacs >=24.4 sets xterm in this mode and define
;; some of the escape sequences but not all of them.
;; (defun jf/character-apply-modifiers (c &rest modifiers)
;;   "Apply modifiers to the character C.
;; MODIFIERS must be a list of symbols amongst (meta control shift).
;; Return an event vector."
;;   (if (memq 'control modifiers) (setq c (if (or (and (<= ?@ c) (<= c ?_))
;;                                                 (and (<= ?a c) (<= c ?z)))
;;                                             (logand c ?\x1f)
;;                                           (logior (lsh 1 26) c))))
;;   (if (memq 'meta modifiers) (setq c (logior (lsh 1 27) c)))
;;   (if (memq 'shift modifiers) (setq c (logior (lsh 1 25) c)))
;;   (vector c))

;; (jf/after xterm
;;   (setq xterm-mouse-mode nil)
;;   (when (and (boundp 'xterm-extra-capabilities) (boundp 'xterm-function-map))
;;     (let ((c 32))
;;       (while (<= c 126)
;;         (mapc (lambda (x)
;;                 (define-key xterm-function-map (format (car x) c)
;;                             (apply 'jf/character-apply-modifiers c (cdr x))))
;;               '(;; with ?.VT100.formatOtherKeys: 0
;;                 ("\e\[27;3;%d~" meta)
;;                 ("\e\[27;5;%d~" control)
;;                 ("\e\[27;6;%d~" control shift)
;;                 ("\e\[27;7;%d~" control meta)
;;                 ("\e\[27;8;%d~" control meta shift)
;;                 ;; with ?.VT100.formatOtherKeys: 1
;;                 ("\e\[%d;3u" meta)
;;                 ("\e\[%d;5u" control)
;;                 ("\e\[%d;6u" control shift)
;;                 ("\e\[%d;7u" control meta)
;;                 ("\e\[%d;8u" control meta shift)))
;;         (setq c (1+ c))))))

(provide 'setup-core)
;; TIPS
;;
;; (1) Un-define a symbol/variable
;; this will make the symbol my-nasty-variable's value void
;; (makunbound 'my-nasty-variable)
;;
;; (2) Un-define a function
;; this will make the symbol my-nasty-function's
;; function definition void
;; (fmakunbound 'my-nasty-function)
;;
;; (3) See a variable value
;; `C-h v`, enter the variable name, Enter
;; Example: `C-h v tooltip-mode`
;;
;; (4) Killing buffers from an emacsclient frame
;; `C-x #`   Kills the buffer in emacsclient frame without killing the frame
;; `C-x 5 0` Kills the emacsclient frame
;;
;; (5) `C-q' is bound to `quoted-insert'
;; Example: Pressing `C-q C-l' inserts the `^l' character (form feed):  
;;
;; (6) The way to figure out how to type a particular key combination or to know
;; what a particular key combination does, do help on a key `C-h k`, and type
;; the keystrokes you're interested in. What Emacs shows in the Help buffer is
;; the string you can pass to the macro 'kbd.
;;
;; (7) How to know what the current major mode is?
;; `C-h v major-mode`
;;
;; (8) Put this line at the top of an anything.gpg file to prevent it from
;; asking for the password on each save
;; -*- epa-file-encrypt-to: ("<MY_EMAIL>") -*-
;;
;; (9) Replace a string with string suffixed with incrementing numbers
;; http://www.reddit.com/r/emacs/comments/355bm0/til_after_so_long_using_emacs/cr1l6gy
;; Let's say you want to replace all the instances of "Here" with "Here1" "Here2" "Here3" ..
;;    1. M-x query-replace-regexp
;;    2. regexp: \bHere\b
;;    3. replacement: Here\,(1+ \#)
;;  \, lets you put in some elisp to evaluate, which is incredibly
;; useful. In the s-expression here, \# is a variable available to
;; query-replace-regexp that holds the number of replacements done so
;; far (i.e. before the first replacement it's 0, before the second
;; replacement it's 1, and so on). To start the numbering at one, we
;; just add one to it with the function 1+.
;;
;; (10) C-x =     <-- `what-cursor-position' (default binding)
;;      C-u C-x = <-- `describe-char'
;;
;; (11) Package for csv navigation: csv-nav: https://github.com/emacsmirror/csv-nav
