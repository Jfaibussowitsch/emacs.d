;;; early-init.el --- early bird  -*- no-byte-compile: t -*-
(setq gc-cons-threshold 100000000) ;; 100 MB

;; Load newer version of .el and .elc if both are available
(setq-default load-prefer-newer t)

;; Silence some silly byte compiler warnings
(setq-default byte-compile-warnings '(not docstrings docstrings-non-ascii-quotes))

;; stop package being initialized automatically
;; (setq package-enable-at-startup nil)

(provide 'early-init)
;;; early-init.el ends here
